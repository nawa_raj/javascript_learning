/*
    The global globalThis property contains the global this value, which is akin to the global object.
*/




console.log("\n----------- globalThis property contain global this ------------------\n");
function canMakeHTTPRequest() {
    return typeof (globalThis.XMLHttpRequest) === 'function';
}


console.log("our function: \n", canMakeHTTPRequest.toString(), "\n");
console.log("canMakeHTTPRequest(): ", canMakeHTTPRequest());          // expected output (in a browser): true
console.log()