//  here we create clear class concept of javascript

class Person {
    constructor(fName, lName) {
        this.firstName = fName;
        this.lastName = lName;
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`
    }
}

const person1 = new Person("nawaraj", "jaishi");
console.log();
console.log("Person First Name: ", person1.firstName);
console.log("Person Last Name: ", person1.lastName);
console.log("Person Full Name: ", person1.getFullName());
console.log();


// class inheritance 
console.log();
console.log("----------- class inheritance -------")
class SuperHero extends Person {
    constructor(fName, lName) {
        super(fName, lName);
        this.isSuperHero = true;
    }

    fightCrime() {
        return "Fighting Crime"
    }
}

const batman = new SuperHero("Harry", "Gauli");

console.log("SuperHero First Name: ", batman.firstName);
console.log("SuperHero Last Name: ", batman.lastName);
console.log("SuperHero Full Name: ", batman.getFullName());
console.log("isSuperHero: ", batman.isSuperHero);
console.log(batman.fightCrime());
console.log("parameter length of SuperHero class: ", SuperHero.length);

// console.log(SuperHero.toString());
console.log();