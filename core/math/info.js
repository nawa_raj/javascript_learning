/*
    Math is a built-in object that has properties and methods for mathematical constants and functions.
    It's not a function object.

    Math works with the Number type. It doesn't work with BigInt.

    Unlike many other global objects, Math is not a constructor.
    All properties and methods of Math are static.
*/

