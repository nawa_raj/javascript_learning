/*
    The Math.SQRT1_2 property represents the square root of 1/2 which is approximately 0.707
*/



console.log('\n----------- Math.SQRT1_2 property of Math Object -----------------\n');

function getRoot1Over2() {
    return Math.SQRT1_2;
}

console.log("our function: \n", getRoot1Over2.toString());
console.log("getRoot1Over2(): ", getRoot1Over2());       // expected output: 0.7071067811865476
console.log("Math.SQRT1_2: ", Math.SQRT1_2);             // expected output: 0.7071067811865476
console.log();