/*
    The Math.LOG2E property represents the base 2 logarithm of e, approximately 1.442
*/


console.log("\n----------- Math.LOG2E property of Math object -----------\n");

function getLog2e() {
    return Math.LOG2E;
}

console.log("our function\n", getLog2e.toString(), "\n");
console.log("getLog2e(): ", getLog2e());        // expected output: 1.4426950408889634
console.log("Math.LOG2E: ", getLog2e());
console.log();