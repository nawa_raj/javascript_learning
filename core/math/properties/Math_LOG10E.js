/*
    The Math.LOG10E property represents the base 10 logarithm of e, approximately 0.434:
*/


console.log("\n------------- Math.LOG10E property of Math Object ------------\n");

function getLog10e() {
    return Math.LOG10E;
}

console.log("our function: \n", getLog10e.toString(), "\n");
console.log("getLog10e(): ", getLog10e());       // expected output: 0.4342944819032518
console.log("Math.LOG10E: ", getLog10e());      // expected output: 0.4342944819032518
console.log();