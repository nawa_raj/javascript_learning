/*
    The Math.LN10 property represents the natural logarithm of 10, approximately 2.302:
*/




console.log("\n----------- Math.LN10 property of Math Object ---------------\n")

function getNatLog10() {
    return Math.LN10;
}

console.log("our function: ", getNatLog10.toString(), "\n");
console.log("getNatLog10(): ", getNatLog10());      // expected output: 2.302585092994046
console.log("Math.LN10: ", Math.LN10);              // expected output: 2.302585092994046
console.log();