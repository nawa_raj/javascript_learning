/*
    The Math.LN2 property represents the natural logarithm of 2, approximately 0.693
*/


console.log("\n---------------- Math.LN2 property of Math object -------------------\n");
function getNatLog2() {
    return Math.LN2;
}

console.log("our function: \n", getNatLog2.toString(), "\n");
console.log("getNatLog2(): ", getNatLog2());        // expected output: 0.6931471805599453
console.log("Math.LN2: ", Math.LN2);
console.log();