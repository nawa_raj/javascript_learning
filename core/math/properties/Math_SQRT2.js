/*
    The Math.SQRT2 property represents the square root of 2, approximately 1.414
*/


console.log("\n--------- Math.SQRT2 property of Math Object ------------\n");

function getRoot2() {
    return Math.SQRT2;
}

console.log("our function: \n", getRoot2.toString(), "\n");
console.log("getRoot2(): ", getRoot2());            // expected output: 1.4142135623730951
console.log("Math.SQRT2: ", Math.SQRT2);            // expected output: 1.4142135623730951
console.log();