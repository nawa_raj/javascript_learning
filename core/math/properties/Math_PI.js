/*
    The Math.PI property represents the ratio of the circumference of a circle to its diameter,
    approximately 3.14159
*/


console.log("\n------------ Math.PI property of Math Object ----------------\n");

function calculateCircumference(radius) {
    return 2 * Math.PI * radius;
}

console.log("our function: \n", calculateCircumference.toString(), "\n");
console.log(calculateCircumference(10));    // expected output: 62.83185307179586
console.log(Math.PI);                       // expected output: 3.141592653589793
console.log();