/*
    The Math.exp() function returns e^x, where x is the argument, and e is Euler's number 
    (also known as Napier's constant), the base of the natural logarithms.

    Syntax
        Math.exp(x)

        where x = A number 
    
    Return Value:
    A number representing e^x, where e is Euler's number and x is the argument.
*/



console.log("\n----------- Math.exe(x) method ----------------\n");

console.log("Math.exp(0): ", Math.exp(0));          // expected output: 1
console.log("Math.exp(1): ", Math.exp(1));          // expected output: 2.718281828459 (approximately)
console.log("Math.exp(-1): ", Math.exp(-1));        // expected output: 0.36787944117144233
console.log("Math.exp(2): ", Math.exp(2));          // expected output: 7.38905609893065
console.log("Math.exp(6): ", Math.exp(6));          // expected output: 403.4287934927351
console.log();


