/*
    The Math.round() function returns the value of a number rounded to the nearest integer.

    Syntax
        Math.round(x)

        where x = a number 


    Return Value:
    The value of the given number rounded to the nearest integer.
*/



console.log("\n----------------- Math.round() static method -----------------\n");

console.log("Math.round(0.9): ", Math.round(0.9));                  // expected output: 1
console.log(
    "Math.round(5.95), Math.round(5.5), Math.round(5.05): \n",
    Math.round(5.95), Math.round(5.5), Math.round(5.05)
);                                                                  // expected output: 6 6 5
console.log(
    "Math.round(-5.05), Math.round(-5.5), Math.round(-5.95): \n",
    Math.round(-5.05), Math.round(-5.5), Math.round(-5.95)
);                                                                  // expected output: -5 -5 -6
console.log();




