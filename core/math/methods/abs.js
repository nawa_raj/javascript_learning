/*
    The Math.abs() function returns the absolute value of a number. 
    That is, it returns x if x is positive or zero, and the negation of x if x is negative.

    simply says: if we get the negative value it change into positive and return it.

    Syntax:
        Math.abs(x)
        x =  any number
    
    Return value
    The absolute value of the given number.
*/


// using abs method...
console.log("\n-------------- Math.abs() method of Math Object -----------\n");

function difference(a, b) {
    return Math.abs(a - b);
}

function normalDifference(a, b) {
    return (a - b);
}

console.log("function using Math.abs() method as return: \n", difference.toString(), "\n");
console.log("function with normal subtraction: \n", normalDifference.toString(), "\n");

console.log("\n------ using Math.abs() method to return our subtracted value -----------\n")
console.log(difference(3, 5));                  // expected output: 2
console.log(difference(5, 3));                  // expected output: 2
console.log(difference(1.23456, 7.89012));      // expected output: 6.6555599999999995
console.log();

console.log("\n---------- using normal subtraction function -----------------\n");
console.log(normalDifference(3, 5));                  // expected output: -2
console.log(normalDifference(5, 3));                  // expected output: 2
console.log(normalDifference(1.23456, 7.89012));      // expected output: -6.6555599999999995
console.log();



console.log("\n------------- Behavior of Math.abs() ----------------\n");
console.log("Math.abs('-1'): ", Math.abs('-1'));            // 1
console.log("Math.abs(-2): ", Math.abs(-2));                // 2
console.log("Math.abs(null): ", Math.abs(null));            // 0
console.log("Math.abs(''): ", Math.abs(''));                // 0
console.log("Math.abs([]): ", Math.abs([]));                // 0
console.log("Math.abs([2]): ", Math.abs([2]));              // 2
console.log("Math.abs([1, 2]): ", Math.abs([1, 2]));        // NaN
console.log("Math.abs({}): ", Math.abs({}));                // NaN
console.log("Math.abs('string'): ", Math.abs('string'));    // NaN
console.log("Math.abs(): ", Math.abs());                    // NaN
console.log()