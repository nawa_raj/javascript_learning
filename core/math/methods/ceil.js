/*
    The Math.ceil() function always rounds a number up to the next largest integer.

    Syntax
        Math.ceil(x)
        where x: number

    Return value
    The smallest integer greater than or equal to the given number.
*/


console.log("\n---------- Math.ceil() method always round up next largest integer --------------\n");

console.log("Math.ceil(.95): ", Math.ceil(.95));                // expected output: 1
console.log("Math.ceil(4): ", Math.ceil(4));                    // expected output: 4
console.log("Math.ceil(7.004): ", Math.ceil(7.004));            // expected output: 8
console.log("Math.ceil(-7.004): ", Math.ceil(-7.004));          // expected output: -7
console.log();


