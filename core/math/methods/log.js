/*
    The Math.log() function returns the natural logarithm (base e) of a number.

    Log Simple Explaination:
        log100 
        = log10(100)
        or, log10(100) = 10*10
        or, log10(100) = 10^2
        now, log100 = 2
    
    But in javascript log represent the base e of arguments i.e. e^argument

    Syntax:
        Math.log(x)

        where, x = a number
*/



console.log("\n--------- Math.log(x) static method -----------\n");

function getBaseLog(x, y) {
    return Math.log(y) / Math.log(x);
}

console.log("our function: ", getBaseLog.toString(), "\n\n");

// 2 x 2 x 2 = 8
console.log("getBaseLog(2, 8): ", getBaseLog(2, 8));      // expected output: 3

// 5 x 5 x 5 x 5 = 625
console.log("getBaseLog(5, 625): ", getBaseLog(5, 625));// expected output: 4

// 10 x 10 = 100
console.log("Math.log(100): ", Math.log(100))        // expected output: 2
console.log()


console.log("Math.log(-1): ", Math.log(-1));        // NaN, out of range
console.log("Math.log(0): ", Math.log(0));          // -Infinity
console.log("Math.log(1): ", Math.log(1));          // 0
console.log("Math.log(10): ", Math.log(10));        // 2.302585092994046
console.log();