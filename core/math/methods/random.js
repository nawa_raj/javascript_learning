/*
    The Math.random() function returns a floating-point, pseudo-random number in the range 0 to less than 1
    (inclusive of 0, but not 1) with approximately uniform distribution over that range — which you can 
    then scale to your desired range. 


    Syntax:
        Math.random()


    Return Value:
    A floating-point, pseudo-random number between 0 (inclusive) and 1 (exclusive).    
 */



console.log("\n------------ Math.random() static method ----------------\n");

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

console.log("Math.random(): ", Math.random(), "\n");


console.log("our function: \n", getRandomInt.toString(), "\n");
console.log("getRandomInt(3): ", getRandomInt(3));      // expected output: 0, 1 or 2
console.log("getRandomInt(1): ", getRandomInt(1));      // expected output: 0
console.log("Math.random(): ", Math.random());          // expected output: a number from 0 to <1
console.log();


console.log("\n----------------------- getting randome values between two values -------------------\n");

const getRandRange = (min, max) => {
    const min1 = Math.ceil(min);
    const max1 = Math.floor(max);

    return Math.floor(Math.random() * (max1 - min1) + min1);
}

console.log("our custom range function: ", getRandRange(2, 8));
console.log();