/*
    The Math.imul() function returns the result of the C-like 32-bit multiplication of the two parameters.

    Syntax:
        Math.imul(a, b)

        Parameter:
            a = number 
            b = number 
    
    Return Value:
    The result of the C-like 32-bit multiplication of the given arguments.
*/


console.log("\n---------------- Math.imul(a, b) static method -----------------\n");

console.log("Math.imul(3, 4): ", Math.imul(3, 4));                           // expected output: 12
console.log("Math.imul(-5, 12): ", Math.imul(-5, 12));                      // expected output: -60
console.log("Math.imul(0xffffffff, 5): ", Math.imul(0xffffffff, 5));        // expected output: -5
console.log("Math.imul(0xfffffffe, 5): ", Math.imul(0xfffffffe, 5));        // expected output: -10
console.log();




console.log("Math.imul(2, 4): ", Math.imul(2, 4));                          // 8
console.log("Math.imul(-1, 8): ", Math.imul(-1, 8));                        // -8
console.log("Math.imul(-2, -2): ", Math.imul(-2, -2));                      // 4
console.log("Math.imul(0xffffffff, 5): ", Math.imul(0xffffffff, 5));        // -5
console.log("Math.imul(0xfffffffe, 5): ", Math.imul(0xfffffffe, 5));        // -10
console.log();