/*
    The Math.expm1() function returns e^x - 1, where x is the argument,
    and e the base of the natural logarithms.

    Syntax
        Math.expm1(x);

        where x = A number.

    Return value
    A number representing e^x - 1, where e is Euler's number and x is the argument.
*/



console.log("\n------------ Math.expm1(x) method ---------------\n");
console.log("Math.expm1(0): ", Math.expm1(0));          // expected output: 0
console.log("Math.expm1(1): ", Math.expm1(1));          // expected output: 1.718281828459045
console.log("Math.expm1(-1): ", Math.expm1(-1));        // expected output: -0.6321205588285577
console.log("Math.expm1(2): ", Math.expm1(2));          // expected output: 6.38905609893065
console.log();