/*
    The Math.sign() function returns either a positive or negative +/- 1, 
    indicating the sign of a number passed into the argument. 
    If the number passed into Math.sign() is 0, it will return a +/- 0. 
    Note that if the number is positive, an explicit (+) will not be returned.

    Syntax:
        Math.sign(x)

        where x: A number. If not a number, it is implicitly converted to one.

    Return value:
    A number representing the sign of the given argument:
    If the argument is positive, returns 1.
    If the argument is negative, returns -1.
    If the argument is positive zero, returns 0.
    If the argument is negative zero, returns -0.
    Otherwise, NaN is returned.
*/




console.log("\n--------- Math.sign() method -------------\n");

console.log("Math.sign(3): ", Math.sign(3));            // expected output: 1
console.log("Math.sign(-3): ", Math.sign(-3));          // expected output: -1
console.log("Math.sign(0): ", Math.sign(0));            // expected output: 0
console.log("Math.sign(-0): ", Math.sign(-0));          // expected output: -0

console.log("\nMath.sign(NaN): ", Math.sign(NaN));        // NaN
console.log("Math.sign('foo'): ", Math.sign('foo'));    // NaN
console.log("Math.sign(): ", Math.sign());              // NaN
console.log();



