/*
    The Math.log10() function returns the base 10 logarithm of a number.

    Syntax:
        Math.log10(x)

        where x = number;  
    
    Return value:
    The base 10 logarithm of the given number. If the number is negative, NaN is returned.
*/


console.log("\n----------- Math.log10(x) static method ----------------\n");

console.log("Math.log10(100000): ", Math.log10(100000));// expected output: 5
console.log("Math.log10(2): ", Math.log10(2));// expected output: 0.3010299956639812
console.log("Math.log10(1): ", Math.log10(1));// expected output: 0
console.log("Math.log10(0): ", Math.log10(0));// expected output: -Infinity
console.log(Math.log(100))   // 4.60...
console.log();

