/*
    The Math.fround() function returns the nearest 32-bit single precision float representation of a Number.

    Syntax:
        Math.fround(doubleFloat)

        Parameters:
        A Number. If the parameter is of a different type, it will get converted to a 
        number or to NaN if it cannot be converted.

    Return Value:
    The nearest 32-bit single precision float representation of the given number.  
    
*/




console.log("\n---------- Math.fround() method --------------\n");
console.log("Math.fround(5.5): ", Math.fround(5.5));            // expected output: 5.5
console.log("Math.fround(5.56): ", Math.fround(5.56));          // expected output: 5.559999942779541
console.log("Math.fround(5.05): ", Math.fround(5.05));          // expected output: 5.050000190734863
console.log("Math.fround(5): ", Math.fround(5));                // expected output: 5
console.log("Math.fround(-5.05): ", Math.fround(-5.05));        // expected output: -5.050000190734863
console.log();




