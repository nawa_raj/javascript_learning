/*
    The Math.pow() static method, given two arguments, base and exponent, returns baseexponent.
    it return the power of those value.

    Syntax:
        Math.pow(base, exponent);

    Return value
    A number representing the given base taken to the power of the given exponent.
*/


console.log("\n------------------- Math.pow(base, exponent) static method ---------------\n");

console.log("Math.pow(7, 3): ", Math.pow(7, 3));            // expected output: 343
console.log("Math.pow(4, 0.5): ", Math.pow(4, 0.5));        // expected output: 2
console.log("Math.pow(7, -2): ", Math.pow(7, -2));          // expected output: 0.02040816326530612 it means => (1/49);
console.log("Math.pow(-7, 0.5): ", Math.pow(-7, 0.5));      // expected output: NaN
console.log();