/*
    The Math.floor() function returns the largest integer less than or equal to a given number.

    it return the integer value not round up the associated floating value.

    Syntax
        Math.floor(x)

        where x = A number.


    Return value
    A number representing the largest integer less than or equal to the specified number.
*/


console.log("\n---------- Math.floor(x) mrthod ------------\n");

console.log("Math.floor(5.95): ", Math.floor(5.95));        // expected output: 5
console.log("Math.floor(5.99): ", Math.floor(5.99));        // expected output: 5
console.log("Math.floor(5.05): ", Math.floor(5.05));        // expected output: 5
console.log("Math.floor(5): ", Math.floor(5));              // expected output: 5
console.log("Math.floor(-5.05): ", Math.floor(-5.05));      // expected output: -6
console.log();
