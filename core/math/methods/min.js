/*
    The static function Math.min() returns the lowest-valued number passed into it,
    or NaN if any parameter isn't a number and can't be converted into one.

    Syntax:
        Math.min()
        Math.min(value0)
        Math.min(value0, value1)
        Math.min(value0, value1, ... , valueN)
    
    
    Return Value:
    The smallest of the given numbers. If any one or more of the parameters cannot be converted into a number, 
    NaN is returned. The result is Infinity if no parameters are provided.
*/


console.log("\n----------------- Math.min(val1, ..., valN) static method -------------\n");

console.log("Math.min(2, 3, 1): ", Math.min(2, 3, 1));          // expected output: 1
console.log("Math.min(-2, -3, -1): ", Math.min(-2, -3, -1));    // expected output: -3
console.log("Math.min(): ", Math.min());                        // expected output: inifinity

const array1 = [2, 3, 1];
console.log("\narray: ", array1, "\n");
console.log(Math.min(...array1));                               // expected output: 1
console.log();