/*
    The Math.hypot() function returns the square root of the sum of squares of its arguments.
    It first square the given parameter and then add those arguments square value after that 
    it do square root of the number.

    Syntax:
        Math.hypot()
        Math.hypot(value0)
        Math.hypot(value0, value1)
        Math.hypot(value0, value1, ... , valueN)

    Return Value:
    Square root of of the sum of sqare of the given arguments. 
*/


console.log("\n----------- Math.hypot() Method --------------------\n");

console.log("Math.hypot(3, 4): ", Math.hypot(3, 4));            // expected output: 5
console.log("Math.hypot(5, 12): ", Math.hypot(5, 12));          // expected output: 13
console.log("Math.hypot(3, 4, 5): ", Math.hypot(3, 4, 5));      // expected output: 7.0710678118654755
console.log("Math.hypot(-5): ", Math.hypot(-5));                // expected output: 5
console.log();


console.log("Math.hypot(): ", Math.hypot());                                // 0
console.log("Math.hypot(NaN): ", Math.hypot(NaN));                          // NaN
console.log("Math.hypot(NaN, Infinity): ", Math.hypot(NaN, Infinity));      // Infinity
console.log("Math.hypot(3, 4, 'foo'): ", Math.hypot(3, 4, 'foo'));          // NaN, since +'foo' => NaN
console.log("Math.hypot(3, 4, '5'): ", Math.hypot(3, 4, '5'));              // 7.0710678118654755, +'5' => 5
console.log("Math.hypot(-3): ", Math.hypot(-3));                            // 3, the same as Math.abs(-3)
console.log();