/*
    The Math.max() function returns the largest of the zero or more numbers given as input parameters,
    or NaN if any parameter isn't a number and can't be converted into one.

    Syntax:
        Math.max()
        Math.max(value0)
        Math.max(value0, value1)
        Math.max(value0, value1, ... , valueN)


    Return Value:
    The largest of the given numbers. If any one or more of the parameters cannot be converted into a number,
    NaN is returned. The result is -Infinity if no parameters are provided.
*/



console.log("\n--------------- Math.max(val1, ..., valN) static method ---------------\n");

console.log("Math.max(1, 3, 3.5, 2): ", Math.max(1, 3, 3.5, 2));        // expected output: 3
console.log("Math.max(-1, -3, -2): ", Math.max(-1, -3, -2));            // expected output: -1
console.log("Math.max(): ", Math.max());                                // infinity

const array1 = [1, 3, 2];
console.log("\nour array: ", array1, "\n");
console.log("Math.max(...array1): ", Math.max(...array1));              // expected output: 3
console.log()