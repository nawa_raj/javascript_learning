/*
    The Math.sqrt() function returns the square root of a number.

    Syntax
        Math.sqrt(x)

        where x = a number;


    Return Value:
    The square root of the given number. If the number is negative, NaN is returned.
*/


console.log("\n-------------- Math.sqrt() static method -------------\n");

function calcHypotenuse(a, b) {
    return (Math.sqrt((a * a) + (b * b)));
}

console.log("our hypotenuse function: \n", calcHypotenuse.toString(), "\n");

console.log("calcHypotenuse(3, 4): ", calcHypotenuse(3, 4));        // expected output: 5
console.log("calcHypotenuse(5, 12): ", calcHypotenuse(5, 12));      // expected output: 13
console.log("calcHypotenuse(0, 0): ", calcHypotenuse(0, 0));        // expected output: 0
console.log("Math.sqrt(16): ", Math.sqrt(16));                      // expected output: 4
console.log();


