/*
    The Math.log2() function returns the base 2 logarithm of a number

    Syntax:
        Math.log2(x)

        where x = a number 

    Return Value:
    The base 2 logarithm of the given number. If the number is negative, NaN is returned.
*/



console.log("\n----------- Math.log2(x) static method -----------------\n");

console.log("Math.log2(3): ", Math.log2(3));        // expected output: 1.584962500721156
console.log("Math.log2(2): ", Math.log2(2));        // expected output: 1
console.log("Math.log2(1): ", Math.log2(1));        // expected output: 0
console.log("Math.log2(0): ", Math.log2(0));        // expected output: -Infinity
console.log("Math.log2(16): ", Math.log2(16));      // expected output: 4
console.log();