/*
    The Math.log1p() function returns the natural logarithm (base e) of 1 + a number
    
    Syntax:
        Math.log1p(x)

        where x = a number;


    Return Values:
    The natural logarithm (base e) of 1 plus the given number. If the number is less than -1, NaN is returned.

*/


console.log("\n------------ Math.log1p() static method --------------\n");

console.log("Math.log1p(1): ", Math.log1p(1));          // expected output: 0.6931471805599453
console.log("Math.log1p(0): ", Math.log1p(0));          // expected output: 0
console.log("Math.log1p(-1): ", Math.log1p(-1));        // expected output: -Infinity
console.log("Math.log1p(-2): ", Math.log1p(-2));        // expected output: NaN
console.log();

