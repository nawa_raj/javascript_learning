/*
    in javascript when we work with string and number then we must be be careful because 
    of the its unusual behaviour.

    the following code demonstrate the context that must be know while working with 
    number and string.

*/


// 
var a = "50";
var b = 10;
var c = 20;
var d = "hello";

console.log("\n----------- work with number and string value ----------------\n");
console.log("orginal value of a: ", a, "\tand its type: ", typeof (a));
console.log("orginal value of b: ", b, "\tand its type: ", typeof (b));
console.log("orginal value of c: ", c, "\tand its type: ", typeof (c));
console.log("orginal value of d: ", d, "\tand its type: ", typeof (d));


console.log("\n---------- now consider the operations ------------\n");
console.log("a + b: ", a + b)       // 5010 because it treated + as a concatination operator for this operation.
console.log("a - b: ", a - b)       // 40 because it treated - operator to perform subtraction operation.

/* 
    3050 because it perform subtraction operation first and then it perform concatination 
    operation to join number and string data.

    it is beacause javascript always do operation left to right.
*/
console.log("b + c + a: ", b + c + a);
console.log("a + b + c: ", a + b + c);      // 502010 because javascript treated + operator as a concatination operator first.

console.log("a - b - c: ", a - b - c);      // 20 because javascript treated - operator as a subtraction operator first.
console.log("a + d: ", a + d);

/* 
    NaN because javascript treated - operator as a subtraction operator first and it subtract 
    one string from another that is not possible that's why it give a NaN.
*/
console.log("a - d: ", a - d);

console.log("d - b: ", d - b);  // NaN because it is not valid operation because we are subtracting number from string.
console.log();