/*
    Number is a primitive wrapper object used to represent and manipulate numbers like 37 or -9.25.
        
    The Number constructor contains constants and methods for working with numbers.
    Values of other types can be converted to numbers using the Number() function.

    we can define number with two different approach.
    1. using constructor 
    2. using literal
*/


console.log("\n--------------- number define using String constructor ---------------\n");

const num1 = new Number(12);

console.log("new Number(12): ", num1);
console.log("num1.valueOf(): ", num1.valueOf());
console.log("typeof(num1): ", typeof (num1));
console.log();