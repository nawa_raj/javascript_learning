/*
    Finite numbers are those numbers which can be countable or not a infinity number.

    The global isFinite() function determines whether the passed value is a finite number.
    If needed, the parameter is first converted to a number.

    Syntax
        isFinite(testValue)

        where testValue =  any;
    
    
    Return Value:
    false if the argument is (or will be coerced to) positive or negative Infinity or NaN or undefined;
    otherwise, true.
*/



console.log("\n--------------- Global isFinite(x) Function ------------\n");

function div(x) {
    if (isFinite(1000 / x)) {
        return 'Number is NOT Infinity.';
    }
    return 'Number is Infinity!';
}

console.log("our function: \n", div.toString(), "\n");

console.log("div(0): ", div(0));        // expected output: "Number is Infinity!""
console.log("div(1): ", div(1));        // expected output: "Number is NOT Infinity."

console.log("\nisFinite(NaN): ", isFinite(NaN));
console.log("isFinite(undefined): ", isFinite(undefined));
console.log("isFinite(Infinity): ", isFinite(Infinity));
console.log("isFinite(20 / 0): ", isFinite(20 / 0));
console.log("isFinite(0): ", isFinite(0));
console.log("isFinite(Math.pow(10, 5)): ", isFinite(Math.pow(10, 5)));
console.log();