/*
    modifying the array element:
*/


// orginal array
var number = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
var number2 = number;


// displaying aray:
console.log("\n------------ modifying array element value -----------------\n");
console.log("Orginal Array number: ", number);
console.log("\ncopying array 'number2 = number': ", number2);
console.log("\n\nnumber[3]: ", number[3]);
console.log("number[5]: ", number[5]);


// modifying orginal array emelent
number[3] = 300;

console.log("\n---------- after number[3] = 300 assign -------------------------\n");
console.log("Array number: ", number);
console.log("\nArray number2: ", number2);

number2[5] = 500;

console.log("\n---------- after number2[5] = 500 assign -------------------------\n");
console.log("Array number: ", number);
console.log("\nArray number2: ", number2);

console.log("\nconclusion: if we change copying array element value that also reflec in orginal array.\n")