/*
    The flatMap() method returns a new array formed by applying a given callback function to each element
    of the array, and then flattening the result by one level. It is identical to a map() 
    followed by a flat() of depth 1, but slightly more efficient than calling those two methods separately.

    syntax:
        flatMap((currentValue, index, array) => { statement } )

    Return value:
    A new array with each element being the result of the callback function and flattened to a depth of 1.

    it doesnot change the orginal array i.e. it return shallow copy of array.
*/


var arr = [1, 2, 3, 4];

console.log("\n----------------- if we use array.map() method ---------------\n");
console.log("orginal array: ", arr);
console.log("arr.map(x => [x, x ** 2]): ", arr.map(x => [x, x ** 2]));
console.log();


console.log("\n----------------- array.flatMap() method ---------------\n");
console.log("arr.flatMap(x => [x, x ** 2]): ", arr.flatMap(x => [x, x ** 2]));
console.log("orginal array after flat: ", arr)
console.log();


console.log("\n-------------- array.flatMap() method unable to flat second level nesting -------------\n");
console.log("arr.flatMap(x => [x, [x ** 2]]): ", arr.flatMap(x => [x, [x ** 2]]))
console.log();