/*
    The reverse() method reverses an array in place. The first array element becomes the last, 
    and the last array element becomes the first.

    syntax: 
        array.reverse();

    Return value
    The reversed array.


    it modified the orginal array.    
*/


let arr = [1, "ram", "gagaran", 23.64, "sagar", 1, 2, 3, 4, 5, 6];



console.log("\n------------ reverse() mehod -----------------\n");
console.log("orginal array: ", arr);

const v = arr.reverse()
console.log("arr.reverse(): ", v)

console.log("\nafter reverse orginal array: ", arr);
console.log();
