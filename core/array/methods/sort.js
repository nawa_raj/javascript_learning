/*
    The sort() method sorts the elements of an array in place and returns the sorted array. 
    The default sort order is ascending, built upon converting the elements into strings, 
    then comparing their sequences of UTF-16 code units values.

    The time and space complexity of the sort cannot be guaranteed as it depends on the implementation.

    Syntax:
        sort((a, b) => { statement } )

    Return value
    The sorted array. Note that the array is sorted in place, and no copy is made.

    it modified the orginal array.
*/


// working with sort method
console.log("\n---------- array.sort() method ------------------\n");

const months = ['March', 'Jan', 'Feb', 'Dec'];

console.log("orginal array: ", months);
console.log("months.sort(): ", months.sort());    // expected output: Array ["Dec", "Feb", "Jan", "March"]
console.log();




console.log("\n---------- by default sort() method sort value as UTF-16 Ascending order -------------\n");

const array1 = [1, 30, 4, 21, 100000];

console.log("orginal array: ", array1);
console.log("array1.sort(): ", array1.sort());  // expected output: Array [1, 100000, 21, 30, 4]
console.log();



// sort number of array by its order 
console.log("\n--------- sort() method --------------\n");

let arr = [2, 1, 89, 500, 56, 789, 3, 4, 5, 1, 5, 6, 9, 90, 7];

function assindingNumberSort(a, b) {
    return a - b;
}

function descendingNumberSort(a, b) {
    return b - a;
}

console.log("orginal array: ", arr);
console.log("ascending sort of array: ", arr.sort(assindingNumberSort));
console.log("orginal array after sorted: ", arr);
console.log();

console.log("decending sort of array: ", arr.sort(descendingNumberSort));
console.log("orginal array after sorted: ", arr);
console.log();





// Arrays of objects can be sorted by comparing the value of one of their properties.
console.log("\n------------ sort by object's property 'name' ------------\n");

const items = [
    { name: 'Edward', value: 21 },
    { name: 'Sharpe', value: 37 },
    { name: 'and', value: 45 },
    { name: 'The', value: -12 },
    { name: 'Magnetic', value: 13 },
    { name: 'Zeros', value: 37 }
];

// it sort item by ascending order
function sortByProperty(a, b, property) {
    const nameA = typeof (a[property]) === "string" ? a[property].toLocaleLowerCase() : a[property];
    const nameB = typeof (b[property]) === "string" ? b[property].toLocaleLowerCase() : b[property];

    if (nameA < nameB) {

        // a is less than b by some ordering criterion
        return -1;

    } else if (nameA > nameB) {

        // a is greater than b by the ordering criterion
        return 1;

    } else {

        // a must be equal to b
        return 0;
    }
};

console.log("orginal array: ", items);
console.log("\nitems.sort(sortByProperty(a, b, 'name')): ", items.sort((a, b) => sortByProperty(a, b, 'name')))
console.log();
