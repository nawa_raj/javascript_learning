/*
    The find() method returns the first element in the provided array that satisfies 
    the provided testing function. If no values satisfy the testing function, 
    undefined is returned.

    Syntax:
        array.find((element, index, array) => {  statement  } );


    Return value
    The first element in the array that satisfies the provided testing function. 
    Otherwise, 'undefined' is returned.

    it doesnot modified orginal array.
*/


// find the value 
let array = [12, 5, 8, 130, 51, 60, 44, 3789, 78, 1, 4, 8, 9, 90, 87]

console.log("\n---------- Find the element greater than 50 in an array with find(callBackFun) method ------------------\n");
console.log("orginal array: ", array, "\n");

let foundValue = array.find((val, ind, arr) => val > 50);

console.log("found value in array array.find(calBackFunc): ", foundValue);
console.log("after filtered orginal array: ", array, "\n");





// not find the value 
console.log("\n---------- find() method return undefined if not found value in an array ------------------\n");
console.log("orginal array: ", array, "\n");

foundValue = array.find((val, ind, arr) => val > 50000);

console.log("found value in array: ", foundValue);
console.log();




// Find an object in an array by one of its properties
const inventory = [
    { name: 'apples', quantity: 2 },
    { name: 'bananas', quantity: 0 },
    { name: 'cherries', quantity: 5 }
];


console.log("\n---------- Find an object in an array by one of its properties ------------------\n");
console.log("orginal array of object: ", inventory, "\n");

function search(array, query) {
    return array.find(({ name }) => name.toLocaleLowerCase() === query.toLocaleLowerCase());
}

console.log("search(inventory, 'apples'): ", search(inventory, 'apples'));
console.log("search(inventory, 'Cherries'): ", search(inventory, "Cherries"));
console.log();




