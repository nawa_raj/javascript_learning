/*
    The some() method tests whether at least one element in the array passes the 
    test implemented by the provided function. It returns true if, in the array, 
    it finds an element for which the provided function returns true; otherwise 
    it returns false. It doesn't modify the array.

    Syntax:
        some((element, index, array) => {  statement } )

    Return value
    true if the callback function returns a truthy value for at least one element in the array.
    Otherwise, false.

    it doesnot modified orginal array.
*/


console.log("\n------------- array.some() method ---------------\n");
const array = [1, 2, 3, 4, 5];

// checks whether an element is even
const even = (element) => element % 2 === 0;

console.log("orginal array: ", array);
console.log("is even number in an array: ", array.some(even));
console.log();



// Checking whether a value exists in an array
console.log("\n---------- Checking whether a value exists in an array with some() method ------------\n");
const fruits = ['apple', 'banana', 'mango', 'guava'];

function checkAvailability(arr, val) {
    return arr.some(arrVal => val === arrVal);
}

console.log("orginal array: ", fruits, "\n");
console.log("checkAvailability(fruits, 'kela'): ", checkAvailability(fruits, 'kela'));   // false
console.log("checkAvailability(fruits, 'banana'): ", checkAvailability(fruits, 'banana')); // true
console.log();