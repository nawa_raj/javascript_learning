/* 
    unshift method add new element from to the begining and return new array length.

    syntax:
        array.unshift(val1, val2, val3, ...valN);

    return value:
    new length of array after unshifted value to the array.

    it modified the orginal array.
*/

const array = [1, 2, 3, 4, 5, 6];

// add 100, 400 at the begining 
console.log("\n------- array.unshift(100, 400, 300) ------------------\n");
console.log("orginal array: ", array, "\n");
let modified = array.unshift(100, 400, 300);

console.log("return value of unshift: ", modified);
console.log("orginal array after unshift: ", array);
console.log()