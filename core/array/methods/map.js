/*
    The map() method creates a new array populated with the results of calling a provided 
    function on every element in the calling array.

    Syntax:
        map((element, index, array) => { statement })

    Return value
    A new array with each element being the result of the callback function.

    it return new array which meet the condition of call back function.
*/



const array1 = [1, 4, 9, 16];
const map1 = array1.map(x => x * 2);

console.log("\n----------- array.map() method -----------\n");
console.log("orginal array: ", array1);
console.log("array1.map(x => x * 2): ", map1);              // Array [2, 8, 18, 32]
console.log("orginal array after map: ", array1);

console.log();