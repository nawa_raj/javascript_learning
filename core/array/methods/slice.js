/*
    slice method used to slice array element and return new shallow array.
    start and end value parameter is given, where end is not inclusive.

    syntax:
        array.slice(startIndex, endIndex) => shallow array.

    this method not modified orginal array.
*/

// array
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000, 30000, 79853, 34];

console.log("\n--------- array slice() method ----------\n");
console.log("orginal array: ", arr, "\n");

console.log("arr.slice(3): ", arr.slice(3));
console.log("arr.slice(undefined, 4): ", arr.slice(undefined, 4));
console.log("arr.slice(0, 4): ", arr.slice(0, 4));

console.log("arr.slice(3, 6): ", arr.slice(3, 6));
console.log("arr.slice(-3): ", arr.slice(-3));
console.log("arr.slice(3, -3): ", arr.slice(3, -3));

console.log("\norginal array: ", arr, "\n");
console.log();