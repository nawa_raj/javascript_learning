/*
    The reduceRight() method applies a function against an accumulator and each value of 
    the array (from right-to-left) to reduce it to a single value.

    Syntax:
        reduceRight((accumulator, currentValue, currentIndex, array) => { statement }, initialValue);

    Return value
    The value that results from running the "reducer" callback function to completion over the entire array.

    it doesnot modified the orginal array.
*/


console.log("\n------------- array.reduceRight() method ---------------\n");
const array1 = [[0, 1], [2, 3], [4, 5]];

let concatArr = array1.reduceRight(
    (accumulator, currentValue) => accumulator.concat(currentValue)
);

console.log("orginal array: ", array1);
console.log("after one level concatnation: ", concatArr);
console.log();




// Counting instances of values in an object
console.log("\n-------------- Counting instances of values in an object -----------\n")
const names = ['Alice', 'Bob', 'Tiff', 'Bruce', 'Alice']

let countedNames = names.reduceRight(function (allNames, name) {
    if (name in allNames) {
        allNames[name]++
    }
    else {
        allNames[name] = 1
    }
    return allNames
}, {})

console.log("orginal array: ", names);
console.log("counted object: ", countedNames);
console.log();