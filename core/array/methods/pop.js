/*
    The pop() method removes the last element from an array and returns that element.
    This method changes the length of the array.

    syntax:
        array.pop();

    return value:
    it return removed value 

    it modified orginal array.
*/


const array = ['angel', 'clown', 'mandarin', 'surgeon', 123, 563734];


// remove first element of array
console.log("\n------- array.pop() ------------------\n");
console.log("orginal array: ", array, "\n");

let modified = array.pop();

console.log("return value of pop: ", modified);
console.log("orginal array after pop: ", array);
console.log()