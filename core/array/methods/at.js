/*
    The at() method takes an integer value and returns the item at that index,
    allowing for positive and negative integers. Negative integers count back 
    from the last item in the array.

    syntax:
        array.at(index);

    return value:
        element at provided index if it is found otherwise it return 'undefined'


    it doesnot change orginal array.
*/

const colors = ['red', 'green', 'blue'];

// finding value by index
console.log("\n------- colors.at(2) ------------------\n");
console.log("orginal colors: ", colors, "\n");

let modified = colors.at(2);

console.log("return value of at(2): ", modified);
console.log("orginal colors after at(2): ", colors);
console.log()





// finding value by index
console.log("\n------- colors.at(5) ------------------\n");
console.log("orginal colors: ", colors, "\n");

modified = colors.at(5);

console.log("return value of at(5): ", modified);
console.log("orginal colors after at(5): ", colors);
console.log()