/*
    The splice() method changes the contents of an array by removing or replacing existing 
    elements and/or adding new elements in place.

    it change orginal array.

    syntax:
        splice(start: number, deleteCount?: number, replaceValue: any): return modified array

        Return value
        An array containing the deleted elements.
        If only one element is removed, an array of one element is returned.
        If no elements are removed, an empty array is returned.

*/

const arr = [1, 2, "ramu", "ram sagar", "gaumul", 23.890];


// Remove 0 (zero) elements before index 2, and insert 'drum'
console.log("\n\n---------- Remove 0 (zero) elements before index 2, and insert 'drum' ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(2, 0, "drum");

console.log("returned value: ", remove);
console.log("array after arr.splice(2, 0, 'drum'): ", arr, "\n");




// Remove 0 (zero) elements before index 2, and insert "drum" and "guitar"
console.log("\n\n---------- Remove 0 (zero) elements before index 2, and insert 'drum' and 'guitar' ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(2, 0, "drum", "guitar");

console.log("returned value: ", remove);
console.log("array after arr.splice(2, 0, 'drum', 'guitar'): ", arr, "\n");




// Remove 1 element at index 2
console.log("\n\n---------- Remove 1 element at index 2 ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(2, 1);

console.log("returned value: ", remove);
console.log("array after arr.splice(2, 1): ", arr, "\n");




// Remove 1 element at index 2, and insert "trumpet"
console.log("\n\n---------- Remove 1 element at index 2, and insert 'trumpet' ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(2, 1, "trumpet");

console.log("returned value: ", remove);
console.log("array after arr.splice(2, 1, 'trumpet'): ", arr, "\n");




// Remove 2 elements from index 0, and insert "parrot", "anemone" and "blue"
console.log("\n\n---------- Remove 2 elements from index 0, and insert 'parrot', 'anemone' and 'blue' ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(0, 2, "parrot", "anemone", "blue");

console.log("returned value: ", remove);
console.log("array after arr.splice(0, 2, 'parrot', 'anemone', 'blue'): ", arr, "\n");




// Remove 2 elements, starting from index 2
console.log("\n\n---------- Remove 2 elements, starting from index 2 ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(2, 2);

console.log("returned value: ", remove);
console.log("array after arr.splice(2, 2): ", arr, "\n");




// Remove 1 element from index -2
console.log("\n\n---------- Remove 1 element from index -2 ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(-2, 1);

console.log("returned value: ", remove);
console.log("array after arr.splice(-2, 1): ", arr, "\n");




// Remove all elements, starting from index 3
console.log("\n\n---------- Remove all elements, starting from index 3 ------------------\n");
console.log("orginal array: ", arr, "\n");

var remove = arr.splice(3);

console.log("returned value: ", remove);
console.log("array after arr.splice(3): ", arr, "\n");
