/*
    The lastIndexOf() method returns the last index at which a given element can be 
    found in the array, or -1 if it is not present. 
    
    The array is searched backwards, starting at fromIndex.

    Syntax:
        lastIndexOf(searchElement, fromIndex)

    
    Return Value:
    The last index of the element in the array; -1 if not found.

    it implement strict equality check "===".
*/


const numbers = [2, 5, 9, 2];
console.log("\n--------- lastIndexOf() method --------------\n")

console.log("orginal array: ", numbers, "\n");

console.log("numbers.lastIndexOf(2): ", numbers.lastIndexOf(2));            // 3
console.log("numbers.lastIndexOf(7): ", numbers.lastIndexOf(7));            // -1
console.log("numbers.lastIndexOf(2, 3): ", numbers.lastIndexOf(2, 3));      // 3
console.log("numbers.lastIndexOf(2, 2): ", numbers.lastIndexOf(2, 2));      // 0
console.log("numbers.lastIndexOf(2, -2): ", numbers.lastIndexOf(2, -2));    // 0
console.log("numbers.lastIndexOf(9, -3): ", numbers.lastIndexOf(9, -3));    // -1
console.log("numbers.lastIndexOf('9', -3): ", numbers.lastIndexOf("9",));   // -1
console.log("numbers.lastIndexOf(9, -3): ", numbers.lastIndexOf(9));        // 2
console.log("numbers.lastIndexOf(2, -1): ", numbers.lastIndexOf(2, -1));    // 3
console.log();