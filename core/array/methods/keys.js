/*
    The keys() method returns a new Array Iterator object that 
    contains the keys for each index in the array.

    Syntax:
    keys()

    Return value
    A new Array iterator object.

    it doesnot modefied the orginal array.
*/


console.log("\n----------- array.keys() method -------------\n");
const array1 = ['a', 'b', 'c'];
console.log("orginal array: ", array1);


const iterator = array1.keys();
console.log("iterator array: ", iterator);

for (let key of iterator) {
    console.log(key);
}
console.log();



console.log("\n----------- abstracting keys using spread operator with array.keys() method -------------\n");
let arr = ["ram", 2, 6, 90, "ganesh", 34.98];

let keysValue = [...arr.keys()];

console.log("[...arr.keys()]: ", keysValue);
console.log();



console.log("\n----------- abstracting keys using Object.keys(array) method -------------\n");

keysValue = Object.keys(arr);

console.log("Object.key(arr): ", keysValue);
console.log();