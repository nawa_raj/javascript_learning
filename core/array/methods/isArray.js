/*
    isArray() method is used to determine passed value is a array or not.
    
    syntax:
        Array.isArray(value)
            return true or false 
*/


// 
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000, 30000, 79853, 34];

console.log("\n--------------- isArray() Method ---------------\n");
console.log("Array.isArray(arr): ", Array.isArray(arr));
console.log("Array.isArray(21, 'hari'): ", Array.isArray(21, "hari"));
console.log("Array.isArray({ name: 'nawaraj' }): ", Array.isArray({ name: "nawaraj" }));
console.log("Array.isArray([]): ", Array.isArray([]));
console.log()