/*
    toString method is used to make a array string

    syntax:
        array.toString()
    
    Return Value:
    it return array element in single string form with seperated by comma.
    
    it doesnot modified the orginal array.
*/


// array
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000, 30000, 79853, 34];

console.log("\n---------- toString() method ------------\n");
console.log("orginal array: ", arr, "\n");

console.log("arr.toString(): ", arr.toString());

console.log("after arr.toString() orginal array: ", arr);

console.log()
