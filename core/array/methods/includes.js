/*
    The includes() method determines whether an array includes a certain value among its entries, 
    returning true or false as appropriate.

    it compare search value in array with strict equality check"===" method. 

    Syntax:
    includes(searchElement, fromIndex)

    Return Value:
    True if value exist in the array otherwise False.

    it doesnot modified orginal array.
*/


console.log("\n------------- searching element in array with includes() method ------------\n");
console.log("[1, 2, 3].includes(2): ", [1, 2, 3].includes(2));              // true
console.log("[1, 2, 3].includes(2, 4): ", [1, 2, 3].includes(2, 4));        // false
console.log("[1, 2, 3].includes(4): ", [1, 2, 3].includes(4))               // false
console.log("[1, 2, 3].includes(3, 3): ", [1, 2, 3].includes(3, 3))         // false
console.log("[1, 2, 3].includes(3, -1): ", [1, 2, 3].includes(3, -1))       // true
console.log("[1, 2, NaN].includes(NaN): ", [1, 2, NaN].includes(NaN))       // true
console.log("['1', '2', '3'].includes(3): ", ["1", "2", "3"].includes(3))   // false
console.log();



//  using negative value
let arr = ['a', 'b', 'c'];

console.log("\n---------- search with fromIndex using includes(searchValue, fromIndex) method ------------\n")

console.log("orginal array: ", arr);
console.log("arr.includes('a', -2): ", arr.includes('a', -2))       // false
console.log("arr.includes('b', -100): ", arr.includes('b', -100))   // true
console.log("arr.includes('c', -100): ", arr.includes('c', -100))   // true
console.log("arr.includes('a', -2): ", arr.includes('a', -1))       // false
console.log();