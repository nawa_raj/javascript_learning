/*
    The flat() method creates a new array with all sub-array elements concatenated into 
    it recursively up to the specified depth.

    Syntax:
        flat(depth)

    Return value
    A new array with the sub-array elements concatenated into it.

    it return the shallow copy of array not modified orginal array.
*/


// flat the single dept array
const arr = [1, 2, [3, 4]];

console.log("\n---------- flat the single dept array ------------------\n");
console.log("orginal array: ", arr);

// To flat single level array
arr.flat();

// is equivalent to
arr.reduce((acc, val) => acc.concat(val), []);

// or with decomposition syntax
const flattened = arr => [].concat(...arr);

console.log("flated array arr.flat(): ", arr.flat());

console.log("after flat array: ", arr, "\n");




// second level depth
console.log("\n----------array.flat(2) ------------------\n");
var arr2 = [0, 1, 2, [[[3, 4]]]];


console.log("orginal array: ", arr2);

let flatValue = arr2.flat(2);

console.log("flated of array.flat(2): ", flatValue);
console.log("after flat array: ", arr2, "\n");
console.log();



// not found case undefined
console.log("\n----------array.flat(3) ------------------\n");
arr2 = [0, 1, 2, [[[3, 4]]]];

console.log("orginal array: ", arr2);

flatValue = arr2.flat(3);

console.log("flated of array.flat(3): ", flatValue);
console.log("after flat array: ", arr2, "\n");