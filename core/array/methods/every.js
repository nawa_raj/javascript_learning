/*
    The every() method tests whether all elements in the array pass the test implemented by the 
    provided function. It returns a Boolean value.

    syntax:
        array.every((element, index, array) => {  statement  } );

    Return value
    true if the callbackFn function returns a truthy value for every array element. 
    Otherwise, false.

    it doesnot change orginal array.
*/

const array1 = [1, 30, 39, 29, 10, 13];

console.log("\n------------------ every() method ------------\n");
console.log("orginal array: ", array1, "\n");

let isPass = array1.every((val) => val < 40);

console.log("is array each element is less than 40: ", isPass);
console.log();



// checking is pass 
console.log("\n------------------ every() method ------------\n");
console.log("orginal array: ", array1, "\n");

isPass = array1.every((val) => val > 20);

console.log("is array each element is greter than 20: ", isPass);
console.log();



// Check if one array is a subset of another array
console.log("\n---------- Check if one array is a subset of another array -----------------\n");
const isSubset = (array1, array2) => array2.every(element => array1.includes(element));

console.log(isSubset([1, 2, 3, 4, 5, 6, 7], [5, 7, 6])); // true
console.log(isSubset([1, 2, 3, 4, 5, 6, 7], [5, 7, 8])); // false
console.log(isSubset([1, 2, 3, 4, 5, 6, 7], [1, 7, 3])); // true
console.log();