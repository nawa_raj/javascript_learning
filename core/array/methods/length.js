/*
    the thength is a property of array which is used to get the length of array.
    
*/

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log("\n---------- Array.prototype.length ----------\n");
console.log("orginal array: ", arr);
console.log("arr.length: ", arr.length);
console.log();