/*
    The entries() method returns a new Array Iterator object that contains the key/value pairs 
    for each index in the array.

    syntax:
        array.entries();

    Return value
    A new Array iterator object.

    it return shallow copy of array i.e. orginal array not modified.
*/


let colors = ['red', 'green', 'blue', 3, 6, 70, 486];


// make iterator object from array.
console.log("\n------- colors.entries() ------------------\n");
console.log("orginal colors: ", colors, "\n");

let iteratorObj = colors.entries();

console.log("iterator object of colors.entries(): ", String(iteratorObj));
// console.log("iteratorObj.next().value: ", iteratorObj.next().value);
// console.log("iteratorObj.next().value: ", iteratorObj.next().value);
console.log("orginal colors: ", colors, "\n");


console.log("\n------ displaying iterator object with for loop --------------\n");
for (let i of iteratorObj) {
    console.log(i)
}
console.log();
