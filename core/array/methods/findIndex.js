/*
    The findIndex() method returns the index of the first element in the array that satisfies 
    the provided testing function. Otherwise, it returns -1, indicating that no element passed the test.

    Syntax:
        array.findIndex((element, index, array) => {  statement  } );


    Return value
    The index of first element in the array that satisfies the provided testing function. 
    Otherwise, -1 is returned.

    it doesnot modified orginal array.
*/


// find the index of element 
let array = [1, 4, 8, 9, 90, 87];


console.log("\n---------- Find first index of element from array with findIndex() method ------------------\n");
console.log("orginal array: ", array, "\n");

let foundValue = array.findIndex((val, ind, arr) => val > 50);

console.log("found index of element whose value is greater than 50: ", foundValue);
console.log("after filtered orginal array: ", array, "\n");




// not found case undefined
console.log("\n---------- Find first index of element from array with findIndex() method ------------------\n");
console.log("orginal array: ", array, "\n");

foundValue = array.findIndex((val, ind, arr) => val > 500);

console.log("found index of element whose value is greater than 500: ", foundValue);
console.log();