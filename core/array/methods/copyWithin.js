/*
    The copyWithin() method shallow copies part of an array to another location in the same array
    and returns it without modifying its length.

    syntax:
        array.copyWithin(target, start, end)

    return Value:
    The modified array.

    it modified the orginal array.
*/


let colors = ['red', 'green', 'blue', 3, 6, 70, 486];


// copying element from index and replace the value at destination.
console.log("\n------- colors.copyWithin(0, 5, 6) ------------------\n");
console.log("orginal colors: ", colors, "\n");

let modified = colors.copyWithin(0, 5, 6);

console.log("return value of colors.copyWithin(0, 5, 6): ", modified);
console.log("orginal colors after copyWithin(): ", colors);
console.log()


colors = ['red', 'green', 'blue', 3, 6, 70, 486];
// copying element from index and replace the value at destination.
console.log("\n------- colors.copyWithin(0, -4) ------------------\n");
console.log("orginal colors: ", colors, "\n");

modified = colors.copyWithin(0, -4);

console.log("return value of colors.copyWithin(0, -4): ", modified);
console.log("orginal colors after copyWithin(): ", colors);
console.log()



colors = ['red', 'green', 'blue', 3, 6, 70, 486];
// copying element from index and replace the value at destination.
console.log("\n------- colors.copyWithin(1, -4, -2) ------------------\n");
console.log("orginal colors: ", colors, "\n");

modified = colors.copyWithin(1, -4, -2);

console.log("return value of colors.copyWithin(0, -4, -2): ", modified);
console.log("orginal colors after copyWithin(): ", colors);
console.log()




colors = ['red', 'green', 'blue', 3, 6, 70, 486];
// copying element from index and replace the value at destination.
console.log("\n------- colors.copyWithin(3, 0) ------------------\n");
console.log("orginal colors: ", colors, "\n");

modified = colors.copyWithin(3, 0);

console.log("return value of colors.copyWithin(3, 0): ", modified);
console.log("orginal colors after copyWithin(): ", colors);
console.log()