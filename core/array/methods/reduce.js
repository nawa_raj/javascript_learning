/*
    The reduce() method executes a user-supplied "reducer" callback function on each element of the array,
    in order, passing in the return value from the calculation on the preceding element.
    The final result of running the reducer across all elements of the array is a single value.

    Syntax:
        reduce((previousValue, currentValue, currentIndex, array) => { statement }, initialValue);


    Return value
    The value that results from running the "reducer" callback function to completion over the entire array.

    it doesnot modified the orginal array.
*/



// working with recuder method
console.log("\n---------- sum of the array element with array.reduce() method -----------------\n");

const array1 = [2, 2, 3, 4];
let initialValue = 0;

const sumWithInitial = array1.reduce(
    (previousValue, currentValue) => previousValue + currentValue,
    initialValue
);


console.log("sum: ", sumWithInitial);
console.log()



// Sum of values in an object array
console.log("\n-------------- sum of values in an object wirh array.reduce() method ----------\n");

initialValue = 0;
const obj = [{ x: 10 }, { x: 234 }, { x: 3667.89 }];


let sum = obj.reduce(
    (previousValue, currentValue) => previousValue + currentValue.x
    , initialValue
)

console.log("orginal array of obj: ", obj);
console.log("The Sum Of the object X' values: ", sum);
console.log()





// Flatten an array of arrays using reduce() method
console.log("\n----------- Flatten an array of arrays using reduce() method ---------\n");

let arr = [[0, 1], [2, 3], [4, 5]];
let flattened = arr.reduce(
    (previousValue, currentValue) => previousValue.concat(currentValue),
    []
)

console.log("orginal array: ", arr);
console.log("Falttened Array: ", flattened);
console.log();




// Counting instances of values in an object
console.log("\n-------------- Counting instances of values in an object -----------\n")
const names = ['Alice', 'Bob', 'Tiff', 'Bruce', 'Alice', 'Tiff', 'tiff']

let countedNames = names.reduce(function (allNames, name) {
    if (name in allNames) {
        allNames[name]++;
    }
    else {
        allNames[name] = 1
    }
    return allNames
}, {})

console.log("orginal array: ", names);
console.log("counted object: ", countedNames);
console.log();


// Counting instances of values in an object
console.log("\n-------------- Counting instances of values in an object with no case sencitiviety -----------\n")
const names1 = ['Alice', 'Bob', 'Tiff', 'Bruce', 'Alice', 'Tiff', 'tiff']
countedNames = names1.reduce(function (allNames, name) {
    // const keys = Object.keys(allNames);

    if (name.toLowerCase() in allNames) {
        allNames[name.toLowerCase()]++;
    }
    else {
        allNames[name.toLowerCase()] = 1
    }
    return allNames
}, {})

console.log("orginal array: ", names);
console.log("counted object: ", countedNames);
console.log();




// Grouping objects by a property
console.log("\n---------Grouping objects by a property -------------\n");

const people = [
    { name: 'Alice', age: 21 },
    { name: 'Max', age: 20 },
    { name: 'Jane', age: 20 }
];

function groupBy(array, property) {
    return array.reduce((acc, obj, ind, arr) => {

        let key = obj[property];

        if (!acc[key]) {
            acc[key] = [];
        }

        acc[key].push(obj);
        return acc;

    }, {})
}

console.log(groupBy(people, "age"));
console.log();




// remove the duplicate value from array
console.log("\n-------- remove duplicate element from array using array.reduce() method ---------------\n");

const myArray = ['a', 'b', 'a', 'b', 'c', 'e', 'e', 'c', 'd', 'd', 'd', 'd'];

let myArrayWithNoDuplicates = myArray.reduce(function (previousValue, currentValue) {
    if (previousValue.indexOf(currentValue) === -1) {
        previousValue.push(currentValue)
    }
    return previousValue
}, [])

console.log("orginal array: ", myArray);
console.log("array after remove duplicate element: \n", myArrayWithNoDuplicates);
console.log();
