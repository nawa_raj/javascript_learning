/*
    The filter() method creates a new array with all elements that pass the test 
    implemented by the provided function.

    syntax:
        filter((element, index, array) => {  statement  } )
    
    
    Return value
    A new array with the elements that pass the test. If no elements pass the test, 
    an empty array will be returned

    it does not modified orginal array.
*/


// Filtering out all small values
let array = [12, 5, 8, 130, 44, 3789, 78, 1, 4, 8, 9, 90, 87]

console.log("\n---------- Filtering out all small values less than 50 ------------------\n");
console.log("orginal array: ", array, "\n");

let filtered = array.filter((val, ind, arr) => val < 50);

console.log("returned array from filter: ", filtered);
console.log("after filtered orginal array: ", array, "\n");




// Find all prime numbers in an array
array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

function isPrime(num) {
    for (let i = 2; num > i; i++) {
        if (num % i == 0) return false;
    }
    return num > 1;
}

console.log("\n---------- Find all prime number in an array ------------------\n");
console.log("orginal array: ", array, "\n");

filtered = array.filter(isPrime);

console.log("returned array from filter: ", filtered);
// console.log("after filtered orginal array: ", array, "\n");
console.log();




// Searching in array
function search(array, query) {
    return array.filter((val) => val.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) !== -1);
    // return array.filter((val) => val.toLocaleLowerCase().includes(query.toLocaleLowerCase()) === true);

}

let fruits = ['apple', 'banana', 'grapes', 'mango', 'orange']

console.log("\n------------- search in array ----------------\n");
console.log("orginal array: ", fruits, "\n");

console.log("search(fruits, 'app'): ", search(fruits, "app"));
console.log("search(fruits, 'an'): ", search(fruits, "an"));
console.log("search(fruits, 'o'): ", search(fruits, "o"));
console.log("search(fruits, 'Apple'): ", search(fruits, "Apple"));
console.log("search(fruits, 'suntala'): ", search(fruits, "suntala"));

console.log();