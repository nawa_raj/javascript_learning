/*
    The values() method returns a new array iterator object that contains the values for each 
    index in the array.

    Return value
    A new Array iterator object.

    it doesnot modified orginal array.
*/

console.log("\n-------- array.values() method ----------------\n");
const array1 = ['a', 'b', 'c'];
const iterator = array1.values();

console.log("orginal array: ", array1);
console.log("\nabstracting values of array\n");

for (const value of iterator) {
    console.log(value);
}

console.log();




console.log("\n----------- abstracting values using spread operator with array.keys() method -------------\n");
let arr = ["ram", 2, 6, 90, "ganesh", 34.98];

let keysValue = [...arr.values()];

console.log("orginal array: ", arr);
console.log("[...arr.keys()]: ", keysValue);
console.log();




console.log("\n----------- abstracting keys using Object.keys(array) method -------------\n");

keysValue = Object.values(arr);

console.log("orginal array: ", arr);
console.log("Object.key(arr): ", keysValue);
console.log();