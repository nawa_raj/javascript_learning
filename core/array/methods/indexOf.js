/*
    The indexOf() method returns the first index at which a given
    element can be found in the array, or -1 if it is not present.

    It implement strict equality check "===" for search.


    syntax:
        array.indexOf(searchElement, fromIndex);

    Return value
    The first index of the element in the array; -1 if not found.

    it doesnot modified the orginal array
*/


let array = ["ram", 23.9, 475, 89, 13242, "sagar", "rita", "sagar"];




// find index of "sagar"
console.log("\n\n---------- array.indexOf('sagar') ------------------\n");
console.log("orginal array: ", array, "\n");

var index = array.indexOf("sagar");

console.log("index of 'sagar' in an array: ", index);
// console.log("array after array.indexOf('sagar'): ", array, "\n");






// find index of "sagar thapa"
console.log("\n\n---------- array.indexOf('sagar thapa') ------------------\n");
console.log("orginal array: ", array, "\n");

var index = array.indexOf("sagar thapa");

console.log("index of 'sagar thapa' in an array: ", index);
// console.log("array after array.indexOf('sagar'): ", array, "\n");




// find index of "sa"
console.log("\n\n---------- array.indexOf('sa') ------------------\n");
console.log("orginal array: ", array, "\n");

var index = array.indexOf("ar");

console.log("index of 'sa' in an array: ", index);
console.log("array after array.indexOf('Sagar'): ", array.indexOf("Sagar"));
console.log("array after array.indexOf('sagar'): ", array.indexOf("sagar"), "\n");