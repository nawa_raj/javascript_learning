/*
    The fill() method changes all elements in an array to a static value, from a start index
    (default 0) to an end index (default array.length). It returns the modified array.

    syntax:
        fill(value, start, end); 

    Return value
    The modified array, filled with value.

    it modified the orginal array.
*/

let array = ["ram", 23.9, 475, 89, 13242, "sagar", "rita", "sagar"];


// fill the array with 4
console.log("\n\n---------- array.fill(4) ------------------\n");
console.log("orginal array: ", array, "\n");

var modified = array.fill(4);

console.log("returned value after array.fill(4): ", modified);
console.log("orginal array after array.fill(4): ", array, "\n");





// Using fill() to create a matrix of all 1
console.log("\n\n---------- Using fill() to create a matrix of all 1 ------------------\n");

const arr = new Array(3);
console.log("orginal array: ", arr, "\n");

for (let i = 0; i < arr.length; i++) {

    // Creating an array of size 4 and filled of 1
    arr[i] = new Array(4).fill(1);
}

console.log("matrix of 1: ", arr, "\n");



// Using fill() to create a matrix of all 1
console.log("\n\n---------- fill() from index 3 to index 8  ------------------\n");
console.log("orginal array: ", array, "\n");
array.fill(2, 3, 7)
console.log("now orginal array: ", array, "\n");
