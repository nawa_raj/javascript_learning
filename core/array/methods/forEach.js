/*
    The forEach() method executes a provided function once for each array element.

    syntax:
        forEach((element, index, array) => { statement })
    
    Return value
    undefined.

    it doesnot change the orginal array.
*/


// array
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000, 30000, 79853, 34];


// display 
console.log("\n------------------ forEach loop with array --------------\n");
console.log("orginal array: ", arr);
console.log();

const val = arr.forEach((val, index, array) => {
    console.log(`index${index}: ${val}`);
    //console.log(array) 
});

// const val = arr.forEach((val, index, array) => { return val; }); // return undefined
// console.log(val);
console.log()




// No operation for uninitialized values (sparse arrays)
console.log("\n------------- No operation for uninitialized values ----------------\n");
let numCallbackRuns = 0;
let arraySparse = [, , , ...arr, , ,];

console.log("orginal array: ", arraySparse);

arraySparse.forEach((element) => {
    console.log({ element });
    numCallbackRuns++;
});

console.log("array length: ", arraySparse.length);
console.log("number of times loop run: ", numCallbackRuns);
console.log();