/*
    The concat() method is used to merge two or more arrays. 
    This method does not change the existing arrays, but instead returns a new array

    syntax:
        array.concat(array1, array2, ...arrayN)
            it return new merged array
    
    it deos not modified orginal array.
*/


const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);

console.log("\n---------- array concat() --------------------\n");
console.log("orginal array1: ", array1);
console.log("orginal array2: ", array2);

console.log("\narray3 = array1.concat(array2): ", array3);
console.log("\nnow original array1: ", array1);
console.log();


// we change array3 value 
array3[1] = 'bcd';

console.log("\n---------- when we cange value of array3[1] = 'bcd' --------------------\n");
console.log("orginal array1: ", array1);
console.log("array3: ", array3);
console.log();


// we change array3 value 
array1[2] = 'cde';
// var array3 = array1.concat(array2);

console.log("\n---------- when we cange value of array1[2] = 'cde' --------------------\n");
console.log("orginal array1: ", array1);
console.log("array3: ", array3);
console.log();





/*
    merge more array
*/

const array4 = [1, 2, 3, 4, 5, 6];

console.log("\n---------- merge more array --------------------\n");
console.log("orginal array1: ", array1);
console.log("orginal array2: ", array2);
console.log("orginal array3: ", array3);
console.log("orginal array4: ", array4);
console.log("\narray4.concat(array3): ", array4.concat(array3));
console.log("\norginal array4: ", array4);
console.log();