/*
    The Array.from() static method creates a new, shallow-copied Array instance 
    from an array-like or iterable object.

    Syntax:
        Array.from(arrayLike, (element, index) => { statement } )

    Return Value: 
    new Shallow array.

    it does not modify the orginal array.
    
*/



console.log("\n------------- generate array from string with Array.form() method -------------\n");
let str = 'foo bar';

console.log("orginal string: ", str)
console.log("Array.from(str): ", Array.from(str));
console.log();



console.log("\n------------- squaring the array element with Array.from() method -------------\n");
let arr = [10, 23, 1, 2, 3, 9, 7, 20,];

console.log("orginal array: ", arr)
console.log("Array.from(arr, (x, ind) => x = x ** 2): \n", Array.from(arr, (x, ind) => x = x ** 2));
console.log();




// if we chaek condition then it return true or false
console.log("\n------------- if we chaek condition then Array.from() method return true or false -------------\n");

console.log("orginal array: ", arr)
console.log("Array.from(arr, (x, ind) => x = x > 5): \n", Array.from(arr, (x, ind) => x = x > 5));
console.log("orginal array: ", arr)
console.log();




// Sequence generator function (commonly referred to as "range", e.g. Clojure, PHP etc)
const range = (stop, start = 0, step = 1) => Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + (i * step));

// Generate numbers range 0..4
console.log("\n-------- custom range function --------------\n");
console.log(range(4, 0, 1))         // [0, 1, 2, 3, 4]
console.log();

