/*
    join method is used to join array element by provided seperator.

    syntax:
        array.join(joinValue);
            it return string of all array element seperated by specified seperator
    
    if seperator is not defined or provided then "," is used to seperate.

    it doesnot modified the orginal oarray.

*/


let arr = [1, "ram", "gagaran", 23.64, "sagar", 1, 2, 3, 4, 5, 6]


console.log("\n-------- join() Method ---------------\n");
console.log("orginal array: ", arr, "\n");
console.log("arr.join(', '): ", arr.join(", "));
console.log("arr.join(' '): ", arr.join(" "));
console.log("arr.join(' - '): ", arr.join(" - "));

console.log("\nafter join our orginal array: ", arr);
console.log();