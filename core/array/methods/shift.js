/*
    The shift() method removes the first element from an array and returns that removed element.
    This method changes the length of the array.

    syntax:
        array.shift()
    
    return value:
    it return shifted element from the first index of array.

    it modified the orginal array.

*/

const array = ['angel', 'clown', 'mandarin', 'surgeon', 123, 563734];


// remove first element of array
console.log("\n------- array.shift() ------------------\n");
console.log("orginal array: ", array, "\n");
let modified = array.shift();

console.log("return value of shift: ", modified);
console.log("orginal array after shift: ", array);
console.log()