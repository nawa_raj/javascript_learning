/*
    The Array.of() method creates a new Array instance from a variable number of arguments,
    regardless of number or type of the arguments.

    Syntax:
        Array.of(element0, element1, ...elementN)

    Return value
    A new Array instance.
*/


// working with of() method.

console.log("\n----------- Array.of() Static Method --------------\n");

let arr = Array.of(7);
let arr1 = Array.of(7, 6, 89, 90, 23, "raju");

console.log("Array.of(7): ", arr);
console.log("Array.of(7, 6, 89, 90, 23, 'raju'): ", arr1);
console.log();


// difference between Array.of() and Array() 
console.log("\n----------- difference between Array.of() and Array() Static Method --------------\n");
let arr2 = Array(7);
let arr3 = Array(7, 6, 89, 90, 23, 'raju');

console.log("Array(7): ", arr2);
console.log("Array(7, 6, 89, 90, 23, 'raju'): ", arr3);
console.log();



// difference between Array.of() and Array() 
console.log("\n----------- new Array() constructor --------------\n");
var number = new Array(1, 2, 3, 4, 5);
var number1 = new Array(5);

console.log("new Array(5): ", number1);
console.log("new Array(1, 2, 3, 4, 5): ", number);

console.log();