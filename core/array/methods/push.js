/*
    push() method is used to add array element from the end of array 
    it is opposite of unshift() method.

    The push() method adds one or more elements to the end of an array and 
    returns the new length of the array.

    syntax:
        array.unshift(val1, val2, val3, ...valN);

    return value:
    new length of array after unshifted value to the array.

    it modified orginal array.

*/


const array = [1, 2, 3, 4, 5, 6];

// add 100, 400 at the begining 
console.log("\n------- array.push(100, 400, 300) ------------------\n");
console.log("orginal array: ", array, "\n");
let modified = array.push(100, 400, 300);

console.log("return value of push: ", modified);
console.log("orginal array after push: ", array);
console.log()