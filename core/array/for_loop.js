/*
    here we are accessing array element with for loop variation.

*/


// let arr = [...Array(50).keys()];
let arr = [1, 5, 8, 90, "hello", { name: "ram", age: 28 }, 100, 500, 600, 10000];


// accessing normal for loop 
console.log("\n---------- iterating array with normal for loop ------------------\n");
console.log("Orginal Array arr: ", arr, "\n");

for (let i = 0; i < arr.length; i++) {
    console.log(`index${i}: ${arr[i]}`);
}

console.log();


// using for in loop 
console.log("\n------------ for in loop iterate over index of array ------------------\n");
console.log("Orginal Array arr: ", arr, "\n");

for (let i in arr) {
    console.log(i);
}

console.log();


// for of loop give value of array;
console.log("\n------------ for of loop iterate over value of array ------------------\n");
console.log("Orginal Array arr: ", arr, "\n");

for (let i of arr) {
    console.log(i);
}

console.log();
