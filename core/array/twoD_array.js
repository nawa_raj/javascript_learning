let arr = [
    ["Rahul", "Dell", 10],
    ["Sonam", "Hp", 300],
    ["Rita", "Lenovo", 50],
]


// iterate through 2d array 
console.log("\n----------- two dimentional array ----------------\n");
console.log("Orginal array: ", arr, "\n");

for (let i = 0; i < arr.length; i++) {

    for (let j = 0; j < arr[i].length; j++) {
        console.log(arr[i][j]);
    }
    console.log();
}

console.log();




// crete 2d array using user input
var row = prompt("Enter How Many Rows You Want? ");
var col = prompt("Enter How Many Columns You Want? ");

var newArr = [];

for (let i = 0; i < row; i++) {
    newArr[i] = [];
}


// assign value to each inner array 
for (let i = 0; i < row; i++) {
    for (let j = 0; j < col; j++) {
        const val = prompt(`Enter value for ${i} row and ${j} column: `);
        newArr[i][j] = val;
    }
}


document.write("<br>-------------<strong>your 2D array is:</strong> ------------------<br>")
document.write(newArr);

console.log("\n------------- your 2D array is: ------------------\n");
console.log(newArr);
