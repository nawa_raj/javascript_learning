/*
    we can use delete keyword to delete spefific array element from existing array 
    but it only delete the value of array does not delete the index as well as its value 
    that cause undefined value in that index.
*/

var number = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

console.log("\n------------ delete array element with delete keyword ----------------\n");
console.log("orginal array: ", number);
console.log("orginal array length: ", number.length);
console.log("number[4]: ", number[4]);

delete number[4];

console.log("\n\nafter delete number[4]: ", number);
console.log("after delete array length: ", number.length);
console.log("after delete number[4]: ", number[4]);