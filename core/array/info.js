/*
    aray is an non perimitive data type. it is used to store large number of data with single identifier.

    array is created with two method in javascript.they are:
        1. Using array constructor 
        2. Using Array Literals
*/


// ---------------- Create Array Using Array Constructor ----------------------------------------------------
document.write("<br>------------ <strong>Create Array Using Array() Constructor</strong> ----------------<br>")
console.log("\n-------------- Create Array Using Array() Constructor ----------------\n");

var number = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
var arr = new Array();

// data/properties assign:
arr[0] = "rahul";
arr[8] = 42;
arr[12] = { name: "hati", age: 78 }

// display result 
document.write("<br><strong>Length of Array 'number'</strong>: ", number.length);
document.write("<br><strong>Type of Array 'typeof(number)'</strong>: ", typeof (number));
document.write("<br><strong>number</strong>: ", number);

document.write("<br><br><strong>arr</strong>: ", arr);
document.write("<br><strong>arr.length</strong>: ", arr.length);
document.write("<br><strong>arr[8]</strong>: ", arr[8]);
document.write("<br><strong>arr[10]</strong>: ", arr[10]);

// console 
console.log("number: ", number);
console.log("Array.prototype: ", Array.prototype);
console.log("number.__proto__: ", number.__proto__);
console.log();






// ------------- Create Array Using Array literals ------------------------------------------------------
document.write("<br><br><br>------------ <strong>Create Array Using Array Literal</strong> ----------------<br>")
console.log("\n-------------- Create Array Using Array Literal ----------------\n");

var number1 = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

var arr1 = [];
arr1[0] = { name: "sathi", gpa: 3.67 };
arr1[1] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
arr1[2] = 2352 - 61378;
arr1[3] = "hey";

// display result 
document.write("<br><strong>Length of Array 'number1'</strong>: ", number1.length);
document.write("<br><strong>Type of Array 'typeof(number1)'</strong>: ", typeof (number1));
document.write("<br><strong>number</strong>: ", number1);


document.write("<br><br><strong>arr1</strong>: ", arr1);
document.write("<br><strong>arr1.length</strong>: ", arr1.length);
document.write("<br><strong>arr1[1]</strong>: ", arr1[1]);
document.write("<br><strong>arr1[0]</strong>: ", arr1[0]);
document.write("<br><strong>arr1[3]</strong>: ", arr1[3]);


// console 
console.log("number1: ", number1);
console.log("Array.prototype: ", Array.prototype);
console.log("number1.__proto__: ", number1.__proto__);
console.log();

// array element modification:
number1[4] = 100;
console.log("\n after modification number1[4]=100: ", number1);


// checking some values to verify 
console.log("\n-------- checking some properties and method of Array Object ---------------\n");

// following all code prove that Array also is a object in javascript 
console.log("number.__proto__ === number1.__proto__ : ", number.__proto__ === number1.__proto__);  // true
console.log("Object.prototype === number1.__proto__.__proto__ : ", Object.prototype === number1.__proto__.__proto__); // true
console.log("Object.prototype === number1.__proto__.__proto__ : ", Object.prototype === number.__proto__.__proto__);  // true
console.log("Object.prototype === Array.prototype.__proto__ : ", Object.prototype === Array.prototype.__proto__);     // true