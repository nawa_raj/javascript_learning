/*
    The isNaN() function determines whether a value is NaN or not. 
    Because coercion inside the isNaN function can be surprising,
    you may alternatively want to use Number.isNaN().

    it check the illegal number or not 

    Syntax:
        isNaN(value)
    
    Return Value:
    true if the given value is NaN; otherwise, false.
*/



console.log("\n--------- Global isNaN() function --------------\n");

function milliseconds(x) {
    if (isNaN(x)) {
        return 'Not a Number!';
    }
    return x * 1000;
}

console.log("our function: ", milliseconds.toString(), "\n\n");
console.log("milliseconds('100F'): ", milliseconds('100F'));                // expected output: "Not a Number!"
console.log("milliseconds('0.0314E+2'): ", milliseconds('0.0314E+2'));      // expected output: 3140
console.log("\n\n");


console.log(isNaN(NaN));       // true
console.log(isNaN(undefined)); // true
console.log(isNaN({}));        // true

console.log(isNaN(true));      // false
console.log(isNaN(null));      // false
console.log(isNaN(37));        // false

// strings
console.log(isNaN('37'));      // false: "37" is converted to the number 37 which is not NaN
console.log(isNaN('37.37'));   // false: "37.37" is converted to the number 37.37 which is not NaN
console.log(isNaN("37,5"));    // true
console.log(isNaN('123ABC'));  // true:  parseInt("123ABC") is 123 but Number("123ABC") is NaN
console.log(isNaN(''));        // false: the empty string is converted to 0 which is not NaN
console.log(isNaN(' '));       // false: a string with spaces is converted to 0 which is not NaN

// dates
console.log(isNaN(new Date()));                // false
console.log(isNaN(new Date().toString()));     // true


// This is a false positive and the reason why console.log(isNaN is not entirely reliable
// true: "blabla" is converted to a number.
// Parsing this as a number fails and returns NaN
console.log(isNaN('blabla'));
