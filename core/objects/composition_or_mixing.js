/* 
    composition or mixing help to inherit usable method or property of super class to 
    child class. With the help of mixing we can eleminate unused method or property of 
    super class to its child.
*/

function assignProperty(target, ...source) {
    Object.assign(target, ...source);
}


//
console.log("\n------------- Composition or Mixing ------------------\n");

// making seperate objects...
var talking = { talk: function () { return "Can Talk" } }
var walking = { walk: function () { return "Can Walk" } }
var eating = { eat: function () { return "Can Eat" } }


// create constructor:
var Human = function () { };
var Robot = function () { };


// now composit or mix properties 
assignProperty(target = Human.prototype, eating, talking, walking);
assignProperty(target = Robot.prototype, talking, walking);


// create object from constructor:
var rahul = new Human();
var robotCup = new Robot();


// accessing properties 
console.log("\n----------- accessing Human Object's properties--------------\n");
console.log(rahul.eat());
console.log(rahul.walk());
console.log(rahul.talk());


console.log("\n----------- accessing Robot Object's properties --------------\n");
console.log(robotCup.walk());
console.log(robotCup.talk());
// console.log(robotCup.eat());
console.log();
