/*
    in javascript object is collection of property, property is the association between 
    name(key) and value pair. 

    object declaration and initialization:
    
    1. using Object Literal:
        var person = {}
    
    2. 

*/


// using object literal:
var person = {};

person['fname'] = "nawaraj";
person['lname'] = "jaishi";
person.age = 24;
person[0] = 20;

// persion."college name" = "kist college";         // invalid 
person["college name"] = "kist college";           // valid 
person.fullName = function () { return `${this.fname} ${this.lname}` }


// decleration and initialization in one place:
var school = {
    name: "test school",
    address: "test address",
    total_stu: 1000,
    total_teacher: 20,
    per_techer_stu: function () { return this.total_stu / this.total_teacher },

}



console.log("\n----------- object decleration with object literal -----------\n");
console.log("our person object: ", person);

console.log();
console.log("accessing college name from object: ", person["college name"])

console.log();
console.log("accessing fullName function method: ", person.fullName())
console.log();

console.log("school object: ", school);
console.log("Per teacher student: ", school.per_techer_stu());
console.log();

