/*
    we are checking is property exist or not 
*/

// using 'typeof' keyword:
function Mobile(name, price, brand) {
    this.name = name;
    this.price = price;
    this.brand = brand;
    this.getDesc = function () {
        return `${this.name} ${this.brand} ${this.price}`
    }
}

const samsung = new Mobile("Samsung", 230, "SAM");
const nokiya = new Mobile("Nokiya", 450, "NOK");

if (typeof nokiya.price !== "undefined") {
    console.log("nokiya.price: exist");
} else {
    console.log("nokiya.price: doesnot exist");
}

// deleting object property
console.log("\n------ nokiya.price deleted ---------------\n")
delete nokiya.price;

if (typeof nokiya.price !== "undefined") {
    console.log("nokiya.price: exist");
} else {
    console.log("nokiya.price: doesnot exist");
}



// using 'in' keyword
if ("getDesc" in samsung) {
    console.log("samsung.getDesc: exist");
} else {
    console.log("samsung.getDesc: doesnot exist");
}

// deleting object property
console.log("\n------ samsung.getDesc deleted ---------------\n")
delete samsung.getDesc;

if ("getDesc" in samsung) {
    console.log("samsung.getDesc: exist");
} else {
    console.log("samsung.getDesc: doesnot exist");
}




// using 'in' keyword
if (samsung.hasOwnProperty('price')) {
    console.log("samsung.price: exist");
} else {
    console.log("samsung.price: doesnot exist");
}

// deleting object property
console.log("\n------ samsung.price deleted ---------------\n")
delete samsung.price;

if (samsung.hasOwnProperty('price')) {
    console.log("samsung.price: exist");
} else {
    console.log("samsung.price: doesnot exist");
}
console.log();
