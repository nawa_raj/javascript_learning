//


// super class 
var Mobile = function () {
    this.a = 10;
}

Mobile.prototype.z = 30;


// child or base class 
var Samsung = function () {
    this.b = 20;
}

// creating object of Samsung calss or constructor:
var s = new Samsung();

// before Mobile constructore inheritance in Samsung
document.write("<br>-------- before Mobile calss' <strong>properties/method inheritance</strong> ---------------------<br>");
document.write("value of <strong>b</strong> from Samsung Object: ", `<strong>${s.b}</strong>`);
document.write("<br>value of <strong>a</strong> from Samsung Object: ", `<strong>${s.a}</strong>`);


// now inherit parent constructor in samsung:
var Samsung1 = function () {
    Mobile.call(this);
    this.b = 20;
}
var s1 = new Samsung1();

// after parent property/method inheritance
document.write("<br><br>-------- after Mobile calss' <strong>properties/method inheritance</strong> ---------------------<br>");
document.write("value of <strong>b</strong> from Samsung Object: ", `<strong>${s1.b}</strong>`);
document.write("<br>value of <strong>a</strong> from Samsung Object: ", `<strong>${s1.a}</strong>`);
document.write("<br>value of <strong>z</strong> from Samsung Object: ", `<strong>${s1.z}</strong>`);

// prototype inheritance 
Samsung1.prototype = Object.create(Mobile.prototype);

// reset Samsung constructor 
Samsung1.prototype.constructor = Samsung1;

var s1 = new Samsung1();


console.log(Mobile.prototype)
// after prototype inheritance 
document.write("<br><br>-------- after Mobile calss' <strong> prototype inheritance </strong> ---------------------<br>");
document.write("value of <strong>b</strong> from Samsung Object: ", `<strong>${s1.b}</strong>`);
document.write("<br>value of <strong>a</strong> from Samsung Object: ", `<strong>${s1.a}</strong>`);
document.write("<br>value of <strong>z</strong> from Samsung Object: ", `<strong>${s1.z}</strong>`);