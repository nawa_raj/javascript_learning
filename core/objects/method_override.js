// we are override the method of constructor function from child object


// parent 
var Mobile = function () {

}

Mobile.prototype.show = function () {
    return "This is Parent Show Method"
}

// child 
var Samsung = function () {

}
// child
var Lenovo = function () {
    this.show = function () {
        return "this is override from lenovo constructor"
    }
}


// inheritance 
Samsung.prototype = Object.create(Mobile.prototype);
Samsung.prototype.constructor = Samsung;
Lenovo.prototype = Object.create(Mobile.prototype);
Lenovo.prototype.constructor = Lenovo;


// override from the constructor property 
// Lenovo.show = function () {
//     return "this is override here too."
// }



// override show method in samsung child constructor:
Samsung.prototype.show = function () { return "this method override show() method of parent from Samsung Child" };


// create child object:
var galaxy = new Samsung();
var phab2 = new Lenovo();





// print child properties which are inherit from parent:
console.log("\n--------- method override in prototype -------------\n");
console.log("galaxy.show(): ", galaxy.show())
console.log("galaxy.__proto__.show(): ", galaxy.__proto__.show())
console.log()

console.log("phab2.show(): ", phab2.show())
console.log("phab2.__proto__.show(): ", phab2.__proto__.show())
console.log()


// override from object instance 
phab2.show = function () {
    return "this is override here too."
}

console.log("phab2.show(): ", phab2.show())
console.log();


