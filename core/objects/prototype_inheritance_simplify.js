// here we basically simplify the prototype inheritance of object/constructor function.


// function which help to create prototype inheritance 
function doPrototypeInheritance(child, parent) {
    child.prototype = Object.create(parent.prototype);
    child.prototype.constructor = child;
};

// parent 
var Mobile = function () { };

Mobile.prototype.getModel = function () { return this.model };


// child constructor function/class
var Samsung = function (price, model) {
    this.price = price;
    this.model = model;
};

// child constructor function/class
var Lenovo = function (model) {
    this.model = model;
};

// inheritance
// Samsung.prototype = Object.create(Mobile.prototype);
// Samsung.prototype.constructor = Samsung;
// Lenovo.prototype = Object.create(Mobile.prototype);
// Lenovo.prototype.constructor = Lenovo;
doPrototypeInheritance(child = Samsung, parent = Mobile);
doPrototypeInheritance(child = Lenovo, Mobile);


// adding one extra method to samsung constructor object prototype 
Samsung.prototype.getPrice = function () { return this.price };


// creating object 
var galaxy = new Samsung(32890, "Galaxy");
var phab2 = new Lenovo("Phab 2");



// access getModel method from both child 
console.log("\n---------- Accessing getModel() method from Child Object ------------\n")
console.log(galaxy.getModel());
console.log(galaxy.getPrice());
console.log(phab2.getModel());
console.log();




// passing arguments to parent calss from child class;
var Mobile = function (model) {
    this.model = model;
}

Mobile.prototype.getModel = function () { return this.model };

// child class 
var Samsung = function (model, price) {
    Mobile.call(this, model);
    this.price = price;
}
// cihld class 
var Lenovo = function (model, price) {
    Mobile.call(this, model);
    this.price = price;
}


// prototype inheritance:
doPrototypeInheritance(Samsung, Mobile);
doPrototypeInheritance(Lenovo, Mobile);


// adding one extra method to Samsung constructor/class 
Samsung.prototype.getPrice = function () { return this.price; };


// creating object of child class 
var galaxy = new Samsung("Galaxy", 30000);
var phab2 = new Lenovo("Phab 2", 45000);


// accessing properties of both child class
console.log("\n--------- accessing child class properties --------------\n");
console.log(galaxy.getPrice())
console.log(galaxy.model)
console.log(galaxy.getModel())
console.log(phab2.getModel())
console.log(phab2.price)
console.log();




