//  we can make a class from default function constructor


var Mobile = function (name, model, price) {
    this.name = name;
    this.brand = "Mobile Brand";
    this.model = model;
    this.price = price;
    this.getDesc = function () { return `${this.name} ${this.brand} ${this.model} ${this.price}` }
}

const nokiya = new Mobile("Nokiya", "25627dsh", 23000);

console.log("\n----------- Create Class Functionality from Default Function Constructor -------------------\n");
console.log(nokiya)
console.log()




// we can do same things using class...
class Mobile1 {
    constructor(name, model, price) {
        this.name = name;
        this.brand = "Mobile Brand";
        this.model = model;
        this.price = price;
        this.getDesc = function (post_by) { return `Post By: '${post_by}'-> ${this.name} ${this.brand} ${this.model} ${this.price}`; };
    }
}

const lg = new Mobile1("LG", "25627dsh", 23000);

console.log("\n----------- Accessing Class properties -------------------\n");
console.log(lg, "\n")

lg.name = "Chnaged to LG";

console.log(lg);
console.log(lg.getDesc("nawaraj Jaishi"))
console.log()





// making private properties...
class Mobile2 {
    constructor(name, model, price) {
        this.name = name;

        // to make property private we need to define variable with let, const, or var keyword...
        var brand = "Mobile Brand";

        this.model = model;
        this.price = price;
        this.getDesc = function (post_by) { return `Post By: '${post_by}'-> ${this.name} ${brand} ${this.model} ${this.price}`; };
    }
}

var xiomi = new Mobile2("xiomi", "25627dsh", 23000);

console.log("\n----------- Making Private Properties in  Class -------------------\n");
console.log(xiomi, "\n")

// it cannot be accessed because brand property is private inside class.
console.log("accessing private properties 'xiomi.brand': ", xiomi.brand);
console.log(xiomi.getDesc("nawaraj Jaishi"))
console.log()






// accessing private property of class from outside
// with the help of public method we can access private property of class.

class Mobile3 {
    constructor(name, model, price) {
        this.name = name;
        this.brand = "Mobile Brand";
        this.model = model;

        // to make property private we need to define variable with let, const, or var keyword...
        this.price = price;

        // with the help of following method we can access price as selling price from outside class.
        this.getSellingPrice = function () { return this.price; };
    }
}

var xiomi = new Mobile3("xiomi", "25627dsh", 23000);

console.log("\n----------- Making Private Properties in  Class -------------------\n");
console.log(xiomi, "\n")

// accessing private property of class.
console.log("accessing private properties xiomi.price with the help of public method of calss xiomi.getSellingPrice():\n", xiomi.getSellingPrice());
console.log()




// prototype in javascript
// we use prototype in javascript to modify the property of class, function constructor...
var Mobile = function (name, model, price) {

    // instance member
    this.name = name;
    this.brand = "Mobile Brand";
    this.model = model;
    this.price = price;
    this.getDesc = function () { return `${this.name} ${this.brand} ${this.model} ${this.price}` }
}

var nokia = new Mobile("Nokia", "25627dsh", 23000);
var samsung = new Mobile("Samsung", "SMJ45", 6197843);

console.log("\n----------- prototype in javascript -------------------\n");
console.log("orginal nokia object: ", nokia, "\n");

// if we specify like this, it only available to nokia object not for samsung
nokia.color = "white";
console.log("changed object nokia: ", nokia, "/n");


// now make color available for all object of Mobile class using prototype...
// prototype member
Mobile.prototype.color = "white";

console.log("\n---------- modified object with color is --------------\n");
console.log(samsung);
console.log(samsung.color, "\n");
console.log(nokia);
console.log();

