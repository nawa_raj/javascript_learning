/*  
    in js we can iterate throught the key or property or istance of the 
    object with for in loop and Object.key

*/


// prototype in javascript
// we use prototype in javascript to modify the property of class, function constructor...
var Mobile = function (name, model, price) {

    // instance member
    this.name = name;
    this.brand = "Mobile Brand";
    this.model = model;
    this.price = price;
    this.getDesc = function () { return `${this.name} ${this.brand} ${this.model} ${this.price}` }
}

var nokia = new Mobile("Nokia", "25627dsh", 23000);
var samsung = new Mobile("Samsung", "SMJ45", 6197843);

console.log("\n----------- obtaining key list of object with Object.key() method -------------------\n");
console.log("orginal nokia object: ", nokia, "\n");

// property member
Mobile.prototype.color = "white";

// we can access only object instance member key list with Object.key(nokia)
// we cannot access 'color' because it is a property member
console.log("accessing keys of 'nokia' object: ", Object.keys(nokia), "keys type: ", typeof (Object.keys(nokia)))



// accessing all property of nokia object with for ...in loop.
console.log("\n----------- accessing all property of nokia object with for...in loop -------------------\n");

// for in loop access all the instance member and property member.
for (let v in nokia) {
    console.log(v);
}
console.log()



