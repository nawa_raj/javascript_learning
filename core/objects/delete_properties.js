/*

    delete property means delete the key and its associated value from the object 
    syntax:
        delete object_name.property_name

*/

// lets see 
const person = {
    name: "nawaraj jaishi",
    age: 34,
    class: "middle class",
    gender: "male",
    is_married: false,
    0: "something "
};

console.log("\n--------- deleting object property ------------------\n");
console.log("orginal object: ", person);
console.log("length of object: ", Object.keys(person).length);

delete person.class;
delete person[0];

console.log("\n\nafter executing 'delete person.class' and 'delete person[0]' command\n");
console.log("array now: ", person);
console.log("length of object now: ", Object.keys(person).length);

console.log();