/*
    we are create object using object constructor:
    syntax:
        var object_name = new Object();
*/


const person = new Object();

person.fname = "nawaraj";
person.lname = "jaishi";
person["age"] = 24;
person[0] = "something";
person.getFullName = function () { return `${this.fname} ${this.lname}` }

console.log("\n---------------- object created by using Object Constructor ----------------\n");
console.log("our person object is: ", person);
console.log()

// accessing object properties:
console.log("person.age: ", person.age)
console.log("person[0]: ", person[0])

// accessing object's method
console.log("---- accessing object method -------------\n");
console.log("person.getFullName(): ", person.getFullName())
console.log("person['getFullName'](): ", person['getFullName']())
console.log();