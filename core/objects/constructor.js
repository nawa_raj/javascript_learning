/*
    in javascript constructor is used to create instance of an object

    one rule is:
    when we create constructor of object constructor name must be in Tital convention:

*/

function Mobile(name, price, brand) {
    this.name = name;
    this.price = price;
    this.brand = brand;
    this.getDesc = function () {
        return `${this.name} ${this.brand} ${this.price}`
    }
}

const samsung = new Mobile("Samsung", 230, "SAM");
const nokiya = new Mobile("Nokiya", 450, "NOK");

console.log("\n------------- constructor --------------\n");

console.log(samsung, "\n")

// samsung objects property access 
console.log("samsung.name: ", samsung.name)
console.log("samsung['brand']: ", samsung['brand'])
console.log("samsung.getDesc(): ", samsung.getDesc(), "\n")


console.log(nokiya);

// accessing nokiya object's properties 
console.log("nokiya.name: ", nokiya.name);
console.log("nokiya['brand']: ", nokiya['brand']);
console.log("nokiya.getDesc(): ", nokiya.getDesc());
console.log();


samsung.brand = "Harilal Brand"
console.log(samsung);