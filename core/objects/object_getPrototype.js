/*

    object prototype is examization:

*/

// finding object prototype...
console.log("\n---------- examine Object Prototype ------------\n")
console.log("Object.prototype: ", Object.prototype)
console.log();

console.log("Object.getPrototypeOf(Object.prototype): ", Object.getPrototypeOf(Object.prototype), "\n");




// define one custom object:
var obj = {};

console.log("\n---------- finding prototype of our custom object ----------------\n");

var pro = Object.getPrototypeOf(obj);

console.log("Object.getPrototypeOf(obj): ", pro, "\n");
console.log("Object.getPrototypeOf(pro): ", Object.getPrototypeOf(pro));
console.log();



// define one custom array object:
var obj = [];

console.log("\n---------- finding prototype of our array object ----------------\n");

var pro = Object.getPrototypeOf(obj);
var secLevelPro = Object.getPrototypeOf(pro)

console.log("Object.getPrototypeOf(obj): ", pro, "\n");
console.log("Object.getPrototypeOf(pro): ", Object.getPrototypeOf(pro));
console.log("Object.getPrototypeOf(pro): ", Object.getPrototypeOf(secLevelPro));



// define one custom string object:
var obj = new String("hari");

console.log("\n---------- finding prototype of our string object ----------------\n");

console.log("our Object property: ", obj)
var pro = Object.getPrototypeOf(obj);
var secLevelPro = Object.getPrototypeOf(pro)

console.log("Object.getPrototypeOf(obj): ", pro, "\n");
console.log("Object.getPrototypeOf(pro): ", Object.getPrototypeOf(pro));
console.log("Object.getPrototypeOf(pro): ", Object.getPrototypeOf(secLevelPro));



// checking working procedures of function prototype or object of function 
function Mobile() {
    this.name = "nawaraj jaishi";
}

console.log("\n----------- checking object of constructor function --------------\n");
console.log(Mobile);

// java compiler add prototype property to the Mobile constructor function. 
console.log("prototype of Mobile Function: ", Object.getPrototypeOf(Mobile.prototype));

var v = Object.getPrototypeOf(Mobile.prototype)
console.log(Object.getPrototypeOf(v));
console.log();




// checking constructor function object instance has object prototype
// which is equal to its constructor's prototype
console.log("\n---------- creating object of constructor function and accessing its prototype as __proto__ ----------------\n")

function Mobile2() {

}

// creating object from Mobile 
var lg = new Mobile2();

console.log("function prototye: ", Mobile2.prototype);
console.log("object's prototype as obj.__proto__: ", lg.__proto__)

// checking function prototype and object __proto__: 
console.log(Object.getPrototypeOf(Mobile2.prototype) === Object.getPrototypeOf(lg.__proto__))

//checking Mobile function constructor and lg object's constructor: 
console.log(Mobile2 === lg.__proto__.constructor)

// mobile function constructor is also and mobile prototype constroctor is same 
console.log(Mobile2 === Mobile2.prototype.constructor);
console.log()