//  accessing object property of function


// 
function Mobile() {

}

var samsung = new Mobile();

// we are accessing property of object which is not present in object or in its prototype object:
console.log("\n----------- accessing property of object which is not present in its\
 object and its prototype object----------------\n");

console.log(samsung.a);
console.log();




// accessing object property which is in its prototype:
Mobile.prototype.a = "hello, I'm here";
// samsung.__proto__.b = "hello, I'm here"          // both are same

console.log("\n---------- accessing object property which present only in its prototype object ------------\n");
console.log("Mobile.prototype.a: ", Mobile.prototype.a);
console.log("Mobile.a: ", Mobile.a);
console.log();



// assigning value into its object 
console.log()
samsung.b = "hello";

console.log("mobile property (samsung): ", samsung);

// when we assing new property to object it is assigned only to its 
// constructor not to its parent prototype object
console.log("mobile prototype property Mobile.prototype: ", Mobile.prototype);
console.log();



// get object property prioriety

function Mobile() {
    this.a = "initially defined"
}

var lg = new Mobile();

console.log("\n--------------- get object property prioriety --------------\n");

// assign a value to Mobile's prototype object:
Mobile.prototype.a = "this is re assigned again from Mobile.prototype.a = this value";

console.log(lg);
console.log();