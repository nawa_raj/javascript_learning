/*
    character classes includes following character for regex.
    ., \d, \D, \w, \W, \s, \S, \t, \r, \n, \f, [\b], \cX, \xhh, \uhhhh,
    \u{hhhh} or \u{hhhhh}, \p{UnicodeProperty}, \P{UnicodeProperty}, \

 */


// console.log("\n------------ regular expression ---------------\n");
let chessStory = 'He played with32 the King in a8 and she moved her Queen in c2.';
let reg = /\w\d/g;

console.log("\n------------ we are finding word with digit -----------------\n")
console.log("orginal string: ", chessStory, "\n");
console.log("our regex: ", reg);
console.log("matches: ", chessStory.match(reg));         // expected output: Array [ 'a8', 'c2']
console.log();




// we are finding word begen with digit
console.log("\n------------ we are finding word begen with digit -----------------\n");

chessStory = 'He played with32 the King in a8 and she moved her Queen in c2.';
reg = /\b\w\d/g;

console.log("our regex: ", reg);
console.log("matches: ", chessStory.match(reg));





// we are finding word without digit 
console.log("\n------------ we are finding word without digit -----------------\n");

chessStory = 'He played with32 the King in a8 and she moved her Queen in c2.';
reg = /\w+\D\s/;

console.log("our regex: ", reg);
console.log("matches: ", chessStory.match(reg));





// Looking for a series of digits
console.log("\n------------ Looking for a series of digits -----------------\n")

var randomData = "015 354 8787 687351 3512 8735";
reg = /\b\d{4}\b/g;


console.log("orginal string: ", randomData, "\n");
console.log("our regex: ", reg);
console.log("matches: ", randomData.match(reg));
console.log();





// Looking for a word (from the latin alphabet) starting with A
console.log("\n------------ Looking for a word (from the latin alphabet) starting with A -----------------\n")

var aliceExcerpt = "I'm sure I'm not Ada,' she said, 'for her hair goes in such long ringlets, and mine doesn't go in ringlets at all.";
reg = /\b[aA]\w+/g;


console.log("orginal string: ", aliceExcerpt, "\n");
console.log("our regex: ", reg);
console.log("matches: ", aliceExcerpt.match(reg));
console.log();






// we are finding unicode character with range
console.log("\n------------ we are finding unicode character with range -----------------\n");

const moods = 'happy 🙂, confused 😕, sad 😢';
reg = /[\u{1F600}-\u{1F64F}]/gu;

console.log("orginal string: ", moods);
console.log("our regex: ", reg);
console.log("matches: ", moods.match(reg));     // expected output: Array ['🙂', '😕', '😢']
console.log();



//
