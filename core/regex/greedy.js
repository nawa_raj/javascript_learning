/*
    when we search somethong in string with repeated character then we use greedy quantifiers. 
*/


//  * quantifier is used to check character may repeatead zero or more times. 

let str = "hey nawaraj are you there";
let reg = /he*y/g;

console.log("\n-------- '*' greedy quantifier allow zero or multiple repetition of character or statement -------------\n");

// * matches one repetition of character 
console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();


// * matches multiple repetition of character 
str = "heeeeeeeeeeeeeey nawaraj are you there";

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();


// * matches 0 repetition of character 
str = "hy nawaraj are you there";

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();


// * not match other than same repetition of character 
str = "heeeay nawaraj are you there";

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // false
console.log(str.match(reg));
console.log();






//  + quantifier is used to check character should repeatead at least one or more times. 
console.log("\n\n\n-------- '+' greedy quantifier allow at least one or multiple repetition of character or statement -------------\n");

str = "hey nawaraj are you there";
reg = /he+y/g;

// + greedy quantifier match at least one or more repetition
console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();


// + greedy quantifier match multiple repetition
str = "heeeeeeeeeeeeeeeeeey nawaraj are you there";

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();


// + greedy quantifier not match zero repetition
str = "hy nawaraj are you there";

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // false
console.log(str.match(reg));
console.log();


// if we want to match string with special character then we use back slash 
str = "hey+ nawaraj are you there";
reg = /hey\+/g;

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);      // true
console.log(str.match(reg));
console.log();