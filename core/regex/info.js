/*
    Regular expressions are patterns used to match character combinations in strings.
    In JavaScript, regular expressions are also objects.

    we can create regex with two mthod:
    1. Using Object Literals 
    2. Using Object Constructor 


*/

console.log("\n----------------- regex in js ------------------\n");
let reg = /a-zA-Z/

console.log("define reg with object literal (/a-zA-Z/): ", reg);
console.log();


reg = new RegExp(/a-zA-Z/);

console.log("define reg with object constructor (new RegExp(/a-zA-Z/)): ", reg);
console.log("prototype of: ", Object.getPrototypeOf(reg));
console.log();
