/*
    followed by can be achieve through following example.
*/


let str = "hey nawaraj jaishi hows the feeling at now?";

// make fallowed by the value which is inside the bracket.
let reg = /nawa(?=raj)/gm;



console.log("\n--------------- match followed by somethong ----------\n");

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();


// change the text 
str = "hey nawahraj jaishi hows the feeling at now?";

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();


// change the text 
str = "hey nawa raj jaishi hows the feeling at now?";

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();


// followed by whitespace 
str = "hey nawa raj jaishi hows the feeling at now?";
reg = /nawa(?= )/gm;

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();





//  match not followed by:
console.log("\n\n\n--------------- matches not followed by somethong ----------\n");

str = "hey nawahimal, ohh, sorry man, nawaraj jaishi how are you?";
reg = /nawa(?!raj)/gm;

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();



// nawaraj not followed by jaishi
str = "hey, nawaraj jaishi how are you? nawaraj ghar";
reg = /nawaraj(?!.*jaishi)/gm;

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();






//  match x only if x is precesed by y. 
console.log("\n\n\n--------------- Lookbehind assertion ----------\n");

str = "hey nawahimal, ohh, sorry man, nawaraj jaishi how are you?";
reg = /(?<=nawa)raj/gm;

console.log(`orginal string: ${str}`, "\tregex: ", reg);        // nawa
console.log(str.match(reg))
console.log();



//  match x only if x is precesed by y. 
console.log("\n\n\n--------------- Lookbehind assertion ----------\n");

str = "hey himalnawa, ohh, sorry man, nawaraj jaishi how are you?";
reg = /(?<! )nawa/gm;

console.log(`orginal string: ${str}`, "\tregex: ", reg);
console.log(str.match(reg))
console.log();