/*
    we can search some string with optional values in string.
*/



//  ? quantifier is used to make it optional 

let str = "hey nawaraj are you there"
let reg = /hea?y/g;

console.log("\n------------ optional syntax or character search ------------------\n");

console.log("orginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


// check the optional value
str = "heay nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();



// check the optional value with another character 
str = "hejy nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


// check the optional value with repeted same value
str = "heaaaaaay nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();



// check the optional value with repeted same value and repeated quantifier
reg = /hea*?y/g;
str = "heaaaaaay nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


str = "hey nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


reg = /nawa?raj?/g;
str = "heaaaaaay nawaraj are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


str = "hey nawra are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();


// when we make the last charracter optional then it coupld be match other than optional character 
reg = /nawa?raj?/g;
str = "heay nawrazz are you there"
console.log("\norginal string: ", `"${str}"`, "\tregex: ", reg);
console.log(str.match(reg));
console.log();

