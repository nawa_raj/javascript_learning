
// we can use the group to record and match leter 
let str = 'This image has a resolution of 1440×900x800 pixels.';
let reg = /([0-9]+){2}/g;

let result = str.match(reg);

console.log("\n---------- we can use the group and use leter for use ---------------\n");
console.log(result);
console.log(result[0], "\t", result[1]);
console.log();


let aliceExcerpt = 'The Caterpillar and Alice looked at each other';
let regexpWithoutE = /\b[a-df-z]+\b/ig;
console.log(aliceExcerpt.match(regexpWithoutE));
// expected output: Array ["and", "at"]




let imageDescription = 'This image has a resolution of 1440×900 pixels.';
let regexpSize = /([0-9]+)×([0-9]+)/;
let match = imageDescription.match(regexpSize);
console.log(`Width: ${match[1]} / Height: ${match[2]}.`);
// expected output: "Width: 1440 / Height: 900."




aliceExcerpt = "There was a long silence after this, and Alice could only hear whispers now and then.";
var regexpVowels = /[AEIOUYaeiouy]/g;

console.log("Number of vowels:", aliceExcerpt.match(regexpVowels).length);
// Number of vowels: 26




// using group 
let personList = `First_Name: John, Last_Name: Doe
First_Name: Jane, Last_Name: Smith`;

let regexpNames = /First_Name: (\w+), Last_Name: (\w+)/mg;

match = regexpNames.exec(personList);

do {
    console.log(`Hello ${match[1]} ${match[2]}`);
} while ((match = regexpNames.exec(personList)) !== null);





// using named group
personList = `First_Name: John, Last_Name: Doe
First_Name: Jane, Last_Name: Smith`;

regexpNames = /First_Name: (?<firstname>\w+), Last_Name: (?<lastname>\w+)/mg;
match = regexpNames.exec(personList);
do {
    console.log(`Hello ${match.groups.firstname} ${match.groups.lastname}`);
} while ((match = regexpNames.exec(personList)) !== null);