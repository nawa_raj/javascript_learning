/*
    boolean value is used define the truthy or falsy value in javascript
    The Boolean object is an object wrapper for a boolean value.
*/


// the following all values treated as false value 
console.log("\n--------- following all values trated as falsy value in boolean --------------\n");
console.log("Boolean(): ", Boolean());
console.log("Boolean(''): ", Boolean(""));
console.log("Boolean(NaN): ", Boolean(NaN));
console.log("Boolean(null): ", Boolean(null));
console.log("Boolean(0): ", Boolean(0));
console.log("Boolean(-0): ", Boolean(-0));
console.log("Boolean(undefined): ", Boolean(undefined));
console.log("Boolean(false): ", Boolean(false));
console.log();



// the following all are treated as truthy value in boolean 
console.log("\n--------- following all are treated as truthy value in boolean  --------------\n");

console.log("Boolean([]): ", Boolean([]));
console.log("Boolean({}): ", Boolean({}));
console.log("Boolean('hari'): ", Boolean("hari"));
console.log("Boolean(12): ", Boolean(12));
console.log("Boolean(-12): ", Boolean(-12));
console.log("Boolean(1): ", Boolean(1));
console.log("Boolean(true): ", Boolean(true));
console.log();



// comparing falsy values:
console.log("\n----------- compare values as boolean -----------\n");
if ([]) { console.log("[] is truthy") }         // logs "[] is truthy"
if ([] == false) { console.log("[] == false") } // logs "[] == false"

console.log()
