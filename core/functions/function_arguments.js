
// simply accessing function parameter with arguments
function displayArgument() {
    return ` arguments 1: ${arguments[0]} \n argument 2: ${arguments[1]}`;
}

console.log();
console.log("-------- accessing parameter as arguments[index]--------------")
console.log();

console.log(displayArgument("nawaraj", "jaishi"));
console.log();


// we can change the function parameter value
function changeArgumentValue() {
    arguments[0] = "Raj";

    return ` argument 1: ${arguments[0]} \n argument 2: ${arguments[1]}`
}
console.log();
console.log("-------- change parameter value with in function as arguments[index] = value --------------")
console.log();

console.log(changeArgumentValue("nawaraj", "jaishi"));
console.log();



// manupulating arguments...
console.log();
console.log("------ converting arguments object into array ----------")
console.log();

function getArguments() {
    // by default arguments are in object format
    let arr = [];

    for (let v of arguments) {
        arr.push(v);
    };

    return arr;
}

console.log(getArguments(1, "ramu", 122231.45, 'hari'));
console.log();

// simplify the arguments using rest operator
console.log("getting rest arguments using rest operator");
console.log()

function getArgumentsSimplify(...args) {
    console.log(args)
};

getArgumentsSimplify(21, 4564, 78, "askjdfklas", { name: "hkasfdjk" })
console.log();



// rest vs arguments
console.log();
console.log("------ ...rest vs arguments ----------------------");
console.log();

function restShow(...rest) {
    console.log("all rest parameters: ", rest);
    console.log("rest[2]: ", rest[2])
    console.log("rest parameter type: ", typeof (rest));
}

function restShow1(a, ...rest) {
    console.log("all rest parameters: ", rest);

    // rest length is decreased because of 'a' parameter is seperated
    // a is trated as first parameter 
    console.log("value of 'a': ", a);
    console.log("length of rest parameters: ", rest.length);
    console.log("rest parameter type: ", typeof (rest));
}

function argumentShow() {
    console.log("all arguments: ", arguments);
    console.log("arguments[2]", arguments[2]);
    console.log("arguments type: ", typeof (arguments));
}

function argumentShow2(a) {
    console.log("all arguments: ", arguments);

    // arguments length is equal to total arguments length
    // a is treate as first parameter of function
    console.log("value of 'a': ", a);
    console.log("length of rest parameters: ", arguments.length);
    console.log("arguments type: ", typeof (arguments));
}


restShow(12, 56, "raju", "rajkhaf", { name: "hari" });
console.log();
restShow1("hari gautam", "hahsdakjldfsa", { age: 56 }, 2134.345);
console.log();

console.log();
argumentShow(12, 56, "raju", "rajkhaf", { name: "hari" });
console.log();
argumentShow2("sakhjdf", "sagar bhandari", 154128, 89.98, { code: "x23274827" })
console.log();



