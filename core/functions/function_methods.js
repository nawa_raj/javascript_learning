/* 
this keyword in javascript:

the javascript this keyword which is used in a function, refers to the object it belongs to 
it makes function resuable by letting you deside the object value
this value is determined entirely by how a function is called

How to determine "this"?
    implicit binding 
    explicit binding
    new binding
    default binding    
*/

// implicit binding:
console.log();
console.log("----------- implicit binding ---------------");
console.log();

const person = {
    name: "nawaraj jaishi",
    sayMyName: function () {
        console.log(`My Name is ${this.name}`)
    }
}

person.sayMyName();
console.log();



// call function from outside with object context with this
console.log();
console.log("----------- with call method of function to call other object as arguments ---------------");
console.log();

function sayMyName() {
    console.log(`My Name is ${this.name}`)
}

// this function call an another object as agruments... and get this as reference and get name value.
sayMyName.call(person);
console.log();


// if we call sayMyName() function without call method this print undefined value
console.log();
console.log("----------- call sayMyName() function without call method ---------------");
console.log();

// at this point this keyword search for global this.name value but it didn't get so it print undefined as name value
// alternatively we can define name variable like this:

// globalThis.name = "harry potter";

sayMyName();
console.log();



// call function from outside with object context with this
console.log();
console.log("----------- function with this keyword to define variables as constructors in class ---------------");
console.log();

function Person(name, age) {
    // this = {}

    this.name = name;
    this.age = age;
}

const p1 = new Person("nawaraj", 23);       // this is a constructor of Person function
const p2 = new Person("raju rimal", 43);    // this is a constructor of Person function

console.log(`object 1: {name : ${p1.name}, age: ${p1.age}}`, "\n",
    `object 2: {name : ${p2.name}, age: ${p2.age}}`)

console.log("lengthh of Person Function: ", Person.length);
console.log("name of function: ", Person.name);
// console.log("name of function: ", Person("ram hari sunaula", 28).name);
console.log();










