/*
    arrow function is the shortest way to define function in javascript
    arrow functions are always anonymous type.

    when there are more then two parameter we need to enclosed within bracket:
        let fun = (a,b)=>{body}
    
    when there is single parameter we don't need to enclosed within bracket:
        let fun = a =>{body}

    when there is no parameter we ned to write blanck bracket:
        let fun = () =>{body}
*/


// simple arrow function...
const fun = () => {
    return "hello, nawaraj jaishi"
}

// function call 
console.log("\n---------- simple arrow function decleration ----------------\n")
console.log(fun());
console.log();


// pass arguments in arrow function 
const add = (a, b) => {
    return a + b;
}

console.log("\n----------- arrow function with parameter ------------\n")
console.log(add(23, 74))
console.log();


// arrow function return another arrow function
const sub = (a) => {
    const c = a * a;
    console.log("value of first param(a*a): ", c);

    return (b) => {

        return `\n(a - b) i.e. (${c} - ${b}): ${c - b}`;
    }
}

console.log("\n----------- arrow function with closure scope ------------\n")
const subtractor10 = sub(10);
const subtractor20 = sub(20)

console.log(subtractor10(74))
console.log(subtractor20(5))
console.log();




// single line arrow function
// we didn't need to write return in single line arrow function.
const singleLineArrowFun = (name) => `Hello, ${name} Welcome in JavaScript Training.`

console.log("\n----------- Single line Arrow function ------------\n")
console.log(singleLineArrowFun("Bob Marly"))

console.log();




// arrow function with single parameter without bracket
const arrow1 = name => `Hello, ${name} Welcome in Comedy Show.`

console.log("\n----------- arrow function with single parameter without bracket ------------\n")
console.log(arrow1("Charli Chaplin"))

console.log();



// arrow function with default parameter
var arrowFun = (age, name = "Nawaraj jaishi") => `Hello, ${name} Welcome!!!\n your age is ${age}`

console.log("\n----------- arrow function with default parameter ------------\n")
console.log(arrowFun(34))
console.log(arrowFun(34, "Roshan Gi"))

console.log();



// arrow function rest parameter
var arrowFun = (age, name = "Nawaraj jaishi", ...rest) => {
    return {
        name: name,
        age: age,
        restParams: rest
    }
}

console.log("\n----------- arrow function with single parameter without bracket ------------\n")
console.log(arrowFun(34))
console.log(arrowFun(34, "Roshan Gi", "raju", { name: "bikesh thakur", age: 28, isFriendly: true }, 34.90, 789709))

console.log();

(a, b) => a + b;