
// recursive function in javascript 
console.log();
console.log("----------- recursive function ---------------");
console.log();

function factorial(number) {
    console.log("start: ", number);

    // if (number < 2) return 1;
    // return number * factorial(number - 1);
    return number < 2 ? 1 : number * factorial(number - 1);
};

console.log("factorial of factorial(5)", factorial(5))
// console.log(factorial.toString());
console.log();