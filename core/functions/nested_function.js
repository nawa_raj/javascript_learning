// nested function in javascript 
function parentFun(name, age) {

    function hello(name) {
        return `Hello ${name}`;
    }

    return `${hello(name)} your age is: ${age}`;
};

console.log("\n----------- nested function ---------------\n");
console.log('parentFun("bikram khatri", 24): ', parentFun("bikram khatri", 24))
console.log();




// return inner function as return of parent it share state of memory
function counter() {
    count = 0;

    function inner() {
        count++;
        return count;
    }
    return inner;
};

const fn = counter();

console.log("\n----------- return inner function as return of parent value---------------\n");

console.log("first time call: ", fn());
console.log("second time call: ", fn());
console.log("third time call: ", fn());
console.log();