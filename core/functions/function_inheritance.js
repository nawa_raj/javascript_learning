// in this file we see the concept of function inheritance

function Person(fName, lName) {
    this.firstName = fName;
    this.lastName = lName;
}

const person1 = new Person("raj", "harry");
const person2 = new Person("gautam", "gaunguli");

person1.getFullName = function () {
    return `${this.firstName} ${this.lastName}`
}

console.log();
console.log(person1.getFullName());
// console.log(person2.getFullName());     // it throw error of this is not a function
console.log();


// using prototype method of function
console.log();
console.log("--------- assigning getFullName function to Person2 function with 'prototype' ----------");
console.log();

function Person2(fName, lName) {
    this.firstName = fName;
    this.lastName = lName;
}

// we use prototype to function to make availability in functioin call 
Person2.prototype.getFullName = function () {
    return `${this.firstName} ${this.lastName}`
}

const person3 = new Person2("raj", "harry");
const person4 = new Person2("gautam", "gaunguli");

console.log(person3.getFullName());
console.log(person4.getFullName());     // this function work correctly
console.log();



// following superHero() function inherit all properties of Person2 function
console.log();

function superHero(fName, lName) {
    // this = {}
    Person2.call(this, fName, lName);

    this.isSuperHero = true;
}
superHero.prototype.fightCrime = function () {
    console.log("Fighting Crime")
}

// this assign all prototype of person2 to the superHero() function.
superHero.prototype = Object.create(Person2.prototype)
superHero.prototype.constructor = superHero;

const batman = new superHero("bad", "man");
console.log(batman.getFullName())
console.log(batman.firstName)
console.log(batman.isSuperHero)
console.log();