/*
    In javascript anonymous function is allow the creation of function which have no specified name.
        -> Can be stored in variable 
        -> Can be Return in a function
        -> Can be pass in a function

    anonymous function shoule be closed with semicolon, 
*/


// simple anonymous function 
const a = function (a, b) {
    return a + b;
};

// function call 
console.log("\n----------- simple anonymous function -------------\n");
console.log("a(34, 56): ", a(34, 56));
console.log()




// anonymous function as arguments
// simple case 
function funHolder(fun) {
    return fun;
}

console.log("\n---------- simple anonymous function as arguments to another function -------------\n")


const abc = funHolder(function () {
    return "Hello Nawaraj!!!"
})

console.log(abc);
console.log(abc());
console.log();



// another way to call anonymous function
function funHolder1(fun) {
    return fun();
}

console.log("\n---------- anotherway to call anonymous function -------------\n")

const bcd = funHolder1(function () {
    return "Hello Nawaraj!!!"
});

// we can call like this
// console.log(bcd);

// or we can call directly
console.log(funHolder1(function () {
    return "Hello Nawaraj!!!"
}));
console.log();




// returning anonymous function 
function returnAnonymousFun(a) {
    return function (b) {
        return a + b;
    };
}

console.log("\n---------- return anonymous function from function -------------\n")
const anonymousfun = returnAnonymousFun(123);
console.log(anonymousfun(342));
console.log();