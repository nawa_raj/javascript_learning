/*

    IIFE stand for immediately Invoked Function Expression. which is invoke automatically without call

    it is also known as self executing anonymous function.

    syntax:
        (function(){statement;})()
        (function(a, b){return a+b})(20, 40)

*/


// simple example:
console.log("\n---------- Immediadelt Invoked Function Expression(IIFE) example ------------\n");

// function 
(function () { console.log("Hello, sir...."); })();

console.log();




// IIFE function with parameter 
console.log("\n---------- IIFE function with parameter ------------\n");

(function (a, b) { console.log(`sum(a + b): ${a + b}`) })(23, 72);

console.log();




// IIFE function with parameter 
console.log("\n---------- Arrow IIFE function with parameter ------------\n");

((a, b) => { console.log(`sum(a + b): ${a + b}`) })(34, 72);

console.log();



// IIFE function with own variable
console.log("\n---------- Arrow IIFE function with own variable ------------\n");

((name) => {

    var year = 16;

    console.log(`language name: ${name}, from ${year} years of Market Capture`)
})("JavaScript");

console.log();