/*

    A Closure is a function having access to the parent scope. It preserves the data from outside.
    A closure is an inner function that has access to the outer (enclosing) function variable.

*/


// lexical scoping example:

function parent() {
    var name = 'Mozilla';   // local variable

    function child() {

        // it is a lexical scoping because child have access to the parent variable name
        console.log(name);
    }

    child();
}

console.log("\n-------- legical Scope -----------------\n");
parent();
console.log();



// closure example 
function makeFunc() {
    var name = 'Mozilla';

    function displayName() {
        console.log(name);
    }

    return displayName;
}

// function expression
var myFunc = makeFunc();

console.log("\n------ closure example-----------\n")
myFunc();
console.log();



// closure function with parameter 
function makeAdder(x) {

    return function (y) {
        return x + y;
    };
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);
console.log("\n----------- closure function with parameter ------------------\n")

console.log(add5(2));  // 7
console.log(add10(2)); // 12

console.log();