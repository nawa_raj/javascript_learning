/*

    a function which return object is called factory function.
    syntax:
        function test(){
        return {
        name:"....",
        age: 23,
        ...
        }
        }

    we can instinciate as like:
    const v = test();
    console.log(v.name);

*/

// simple factory function:
function person() {
    return {
        name: "nawaraj jaishi",
        age: 34,
        contact: [98753164263, 9858751751263],
        address: { state: "7", district: "bajura", munciplity: "budhinanda", wardNo: 2 },
        primary_contact: function () { return this.contact[0] }
    }
};

const personObj = person();
console.log("\n---------- factory function demo ------------\n")
console.log("factory function 'person' return object: ", personObj, "\n");

// accessing factory function object's method
console.log("accessing factory function return object method: ", personObj.primary_contact())

// accessing factory function object's properties:
console.log("\n", "--------accessing factory function's  object's properties ------------\n")
console.log(personObj.name)
console.log(personObj.contact)
console.log(personObj.address)
console.log()



// parameterized factory function:
function person1(name, age, contact, address) {
    return {
        name: name,
        age: age,
        contact: contact,
        address: address,
        getPrimaryContact: function (key) {
            return this.contact[key] ? this.contact[key] : "not fund";
        },
        getAddress: function (key) {
            return this.address[key] ? this.address[key] : "not found";
        }
    }
}

const obj = person1(
    "nawaraj jaishi",
    23,
    contact = [9851265182, 62147812861287, 78937021],
    address = { state: "7", district: "bajura", munciplity: "budhinanda", wardNo: 2 }
)

console.log("\n----------- accessing parameterized factory function return object --------------\n");

// accessing object's properties
console.log(obj.name)
console.log(obj.contact)
console.log(obj.address, "\n")

// accessing object's method 
console.log(obj.getAddress("munciplity"));
console.log(obj.getAddress("district"));
console.log(obj.getAddress("city"), "\n");


console.log(obj.getPrimaryContact(0));
console.log(obj.getPrimaryContact(1));
console.log(obj.getPrimaryContact(7));
console.log()

obj.name = "harilal";
console.log(obj, "\n");
