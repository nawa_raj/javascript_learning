/*

in javascript there are many types of loops available 
which make iteration easier in javascript.


to run this code with node js we need to run following command:
    node <filename>.js

*/


// for loop (for statement): simple for loop in javascript which can be used in anywhere.

console.log();
console.log("------------- Simple for loop -----------------");
for (let i = 0; i < 10; i++) {
    console.log(i);
};
console.log();


// omit initialization block
console.log("omit initialization block");

var i = 0;
for (; i < 9; i++) {
    console.log(i);
}


// omitting condition block
console.log("omit condition block");

for (let i = 0; ; i++) {
    console.log(i);
    if (i > 3) break;
};



// omit all expression
console.log("omit all expression/block");

i = 0;

for (; ;) {
    if (i > 3) break;
    console.log(i);
    i++;
}
console.log();


// do while statement is executed once even if the condition is false
console.log()
console.log("------------- do while loop -----------------");

i = 0;
var result = 0;

do {
    result += i;
    i++;
} while (i < 10);

console.log();
console.log("sum of number form 0 to 10: ", result);
console.log();



// while statement is run till its condition is not true.
console.log();
console.log("while loop");

i = 1;
result = 1;

while (i < 10) {
    result *= i;
    i++;
};
console.log();
console.log("multiplication of number from 1 to 10: ", result);
console.log();





/*

The "labeled" statement can be used with break or continue statements. 
It is prefixing a statement with an identifier which you can refer to.

simply, it is the loop variable identifier and provide theh functionality to refer it.

*/

// "label" in iterator/loops
console.log();
console.log("used label in for loop to indentify the loop");

var str = "";

loop1:
for (let i = 0; i < 10; i++) {
    if (i === 4) {
        continue loop1;
    }
    str += i;
}

console.log();
console.log("string value str: ", str);
console.log();




/* 

Use the "break" statement to terminate a loop, switch, or in conjunction with a labeled statement.

*/

// break statement:
console.log();
console.log("-------- break statement with while(true) -------------------");

i = 0;

while (true) {

    if (i > 10) break;
    console.log(i);
    i++;
}

console.log();





/* 

The "continue" statement can be used to restart a while, do-while, for, or label statement.
This simply skipe the checking iteration and go forward...

*/

// "continue" statement:
console.log();
console.log("-------- continue statement with for loop -------------------");

str = "";

for (let i = 0; i < 10; i++) {
    if (i == '7') continue;
    str += i;
}

console.log();
console.log("this omit '7' in iteration: ", str);
console.log();



/*

The for...in statement iterates a specified variable over all the enumerable properties of an object.
for...in loop give us key of the object.

*/

// "for...in loop" statement:
console.log();
console.log("--------- for in loop(it give key of the objcet) -----------");

var object = { name: "nawaraj jaishi", age: 20, gpa: 3.5 };

// we cannot use for...of loop with object data type 
for (const key in object) {
    console.log(`${key}: `, object[key]);
};

console.log();





/*

The for...of statement creates a loop Iterating over iterable objects 
(including Array, Map, Set, arguments object and so on), invoking a custom iteration hook with statements
to be executed for the value of each distinct property.

this loop can usable for iterable objects and it give the value of object..

*/

// "for...of loop" statement:
console.log();
console.log("--------- for...of loop(it give value of iterable object) -----------");

var arr = ["ramu", 1, 3, "hari", 2, 809, { name: "nawaraj jaishi" }, 122431.956879, -567.63827];

for (const val of arr) {
    console.log(val);
};

console.log();


// "for...in loop" with array: it give key of data i.e index
console.log("if we use for...in loop with array it gives index number of value in an array")
console.log()

for (const key in arr) {
    console.log(key);
}
console.log();

console.log("accessing array value using for...in loop");
console.log();

for (const key in arr) {
    console.log(`index ${key}: `, arr[key]);
}

