/*
    The global NaN property is a value representing Not-A-Number.
    NaN is a property of the global object. In other words, it is a variable in global scope.

    The initial value of NaN is Not-A-Number — the same as the value of Number.NaN.
    In modern browsers, NaN is a non-configurable, non-writable property.
    Even when this is not the case, avoid overriding it. It is rather rare to use NaN in a program.

    NaN never compare equal to anything, even itself.
*/



// 
console.log("\n---------------- NaN global object property --------------------\n");

function sanitise(x) {
    if (isNaN(x)) {
        return NaN;
    }
    return x;
}
console.log("our function is: \n", sanitise.toString(), "\n")
console.log("sanitise('1'): ", sanitise('1'));                      // expected output: "1"
console.log("sanitise('NotANumber'): ", sanitise('NotANumber'));    // expected output: NaN
console.log();


console.log("\n---------- NaN is not equal to anything and even itself -------------\n");
console.log("NaN === NaN", NaN === NaN);
console.log("NaN == NaN", NaN == NaN);
console.log("NaN == 30/'hello'", NaN == (30 / "hello"), "\ttype of (30 / 'hello'): ", (30 / "hello"));
console.log();