/*
    here we define class inheritance in js 
*/


// base class 
class Father {
    showFMoney() {
        return "Father Money";
    }
}

// child/derived class 
class Son extends Father {
    constructor() {
        super()
        this.name = "test"
    }
    showSMoney() {
        return "Son Money";
    }
}

// multilevel inheritance in class 
class GrandSon extends Son {
    showGDMoney() {
        return "Grand Son Money";
    }
}



// create class object 
var rakesh = new Son();
var ragav = new GrandSon();

console.log("\n----------- class inheritance --------------\n");

// first level inheritance 
console.log("rakesh.showFMoney(): ", rakesh.showFMoney())
console.log("rakesh.showSMoney(): ", rakesh.showSMoney())

// multi level inheritance
console.log();
console.log("ragav.showFMoney(): ", ragav.showFMoney())
console.log("ragav.showSMoney(): ", ragav.showSMoney())
console.log("ragav.showGDMoney(): ", ragav.showGDMoney())



console.log("\n\n----------- class prototype inheritance investigation  --------------\n");
console.log("ragav.__proto__: ", ragav.__proto__);
console.log("ragav.__proto__.__proto__: ", ragav.__proto__.__proto__);
console.log("ragav.__proto__.__proto__.__proto__: ", ragav.__proto__.__proto__.__proto__);
console.log("ragav.__proto__.__proto__.__proto__.__proto__: ", ragav.__proto__.__proto__.__proto__.__proto__);

