

// super calss
class Father {
    show() {
        return "super class"
    }
}

// derived class 
class Son extends Father {
    show() {
        return "child class"
    }
}


// create class object 
var s = new Son();



// display with write 
document.write("s.show(): ", s.show(), "<br>");


// display result 
console.log("\n------------- super class --------------\n");

// console.log("Father: ", Father, "\n");
console.log("Father.prototype: ", Father.prototype);
console.log();



console.log("\n------------- child class --------------\n");
// console.log("Son: ", Son);
console.log("Son.prototype: ", Son.prototype);
console.log();




console.log("\n******************* with custom constructor *******************\n");
// with custom constructor:
// super calss
class Father1 {
    show() {
        return "super class"
    }
}

// derived class 
class Son1 extends Father1 {
    constructor() {
        super();
        this.a = 236;
    }

    static disp() {
        return "this is a static method."
    }

    show() {
        return "child class"
    }
}


// create class object 
var s = new Son1();



// display with write 
document.write("s.show(): ", s.show(), "<br>");


// display result 
console.log("\n------------- super class --------------\n");

// console.log("Father: ", Father1, "\n");
console.log("Father.prototype: ", Father1.prototype);
console.log();



console.log("\n------------- child class --------------\n");
// console.log("Son: ", Son1);
console.log("Son.disp(): ", Son1.disp());
console.log("Son.prototype: ", Son1.prototype);
console.log();

// it cause error because static mehod or variable cannot access from class object
// console.log("s.disp(): ", s.disp())

console.log("Son1.prototype === Father1.prototype", Son1.prototype === Father1.prototype)
console.log("Son1.prototype.__proto__ === Father1.prototype", Son1.prototype.__proto__ === Father1.prototype)
console.log("s.__proto__: ", s.__proto__);
console.log();


// var lcb = {
//     name: "nawaraj jaishi",
//     age: 34
// }

// console.log(Object.entries(lcb));