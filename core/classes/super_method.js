
// pass arguments to parent class using default super calss
class Father {
    constructor(money) {
        this.money = money;
    }

    // prototype member 
    getDetail() {
        return `I'm Father have Rs. ${this.money} for my child.`
    }
}

// derived class 
class Son extends Father {
    showSMoney() {
        return `\tI'm Son, I haven't any money in my pocket yet \n\t but i can use father money Rs. ${this.money} for my expenses.`
    }
}


// create object of class 
var rahul = new Son(20000);


// showing result 
console.log("\n----------- pass arguments to parent calss using default constructor -----------\n");
console.log("rahul.getDetail(): ", rahul.getDetail());
console.log("rahul.showSMoney(): ", "\n", rahul.showSMoney());
console.log();






// call super class from derived class constructor:
class Father1 {
    constructor(money) {
        this.money = money;
    }

    // prototype member 
    getDetail() {
        return `I'm Father have Rs. ${this.money} for my child.`
    }
}

// derived class 
class Son1 extends Father1 {
    constructor(my_money, father_money) {

        // call super calss first 
        super(father_money);

        // local property members
        this.my_money = my_money;
    }

    showSMoney() {
        return `\tI'm Son, I have Rs.${this.my_money} in my pocket yet \n\t and I can also use father money Rs. ${this.money} for my expenses.`
    }
}


// create object of class 
var rahul1 = new Son1(2000, 45000);


// showing result 
console.log("\n----------- call super(arguments) from child class -----------\n");
console.log("rahul1.getDetail(): ", rahul1.getDetail());
console.log("rahul1.showSMoney(): ", "\n", rahul1.showSMoney());
console.log();