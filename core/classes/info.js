/*
    classed are special type of function in javascript.
*/

class Mobile {
    constructor() {
        this.name = "mobile";
        this.show = function () { return `this is mobile class instance member`; }
    }

    display(message) {
        this.name = message;
        return `your input is: ${this.name}`
        // return "this is a class prototype member.";
    }
}

// object create 
var samsung = new Mobile();


// display property of class 
// document.write(`--------- <strong>Accessing Class Properties </strong>--------------<br> `)
// document.write(samsung.name, "<br>");
// document.write(samsung.show(), "<br>");
// document.write(samsung.display("testing mobile name"), "<br>");

console.log("\n-------------- investigating prototype of class and its object -------------\n")

console.log("class's prototype investigation.")
console.log("Mobile: ", Mobile)
console.log("Mobile.prototype", Mobile.prototype);
console.log("Mobile.prototype.__proto__", Mobile.prototype.__proto__);

console.log("\n\nclass's object prototype investigation.")
console.log("samsung: ", samsung)
console.log("samsung.__proto__", samsung.__proto__);
console.log("samsung.__proto__.__proto__", samsung.__proto__.__proto__);
console.log("Mobile.prototype.__proto__ === samsung.__proto__.__proto__ ?", Mobile.prototype.__proto__ === samsung.__proto__.__proto__)




/*
     if we not define a constructor of class there will be default non parameterized constructor.
     that constructor do all the things when we define in our constructor 

     when we define constructor in js that is called override constructor.

*/


// default constructor 
class Human {
    // name = "nawaraj";

    show() {
        return `I'm the default show message.`
    }
}


// create object 
const person = new Human();
// document.write("<br><br>------------- <strong>Default Constructor</strong> ------------<br>");
// document.write(person.show(), "<br>");

console.log("\n--------- default constructor ---------------\n");
console.log("person: ", person);





// parameterized class/constructor 
class Human1 {
    constructor(name, age, income) {
        this.name = name;
        this.age = age;
        this.income = income;
        this.detail = function () { return { name: this.name, age: this.age } }
    }

    welcome() {
        return `Hi, ${this.name} you are ${this.age} years old and your income is Rs.${this.income}`
    }
}


// create object
// const person1 = new Human1("nawaraj jaishi", 24, 24000);
// document.write("<br><br>------------- <strong>parameterized class/constructor</strong> ------------<br>");
// document.write(person1.welcome(), "<br>");
// document.write("<strong>person name: </strong>", person1.name, "<br>");
// document.write("<strong>person age: </strong>", person1.age, "<br>");
// document.write("<strong>person income: </strong>", person1.income, "<br>");

// console.log("\n--------- parameterized class/constructor ---------------\n");
// console.log("person1: ", person1);
