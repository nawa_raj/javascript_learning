/*

there are two types of type conversion in javascript which are as follows:

    1. Implicit Conversion: 
        It is also known as type coercion where javascript itself will automatically convert
        the type.
    
    2. Explicit Conversion:
        Where we manually convert the type.

*/


console.log()
console.log("---------- Implicit Type Conversion ------------");
console.log();


// it convert all into string while add 
console.log("3 + '3': ", 3 + '3', "\t type: ", typeof (3 + '3'));
console.log("'3' + 3: ", '3' + 3, "\t type: ", typeof ('3' + 3));
console.log("'3' - 3: ", '3' - 3, "\t type: ", typeof ('3' - 3));
console.log("'3' - '3': ", '3' - '3', "\t type: ", typeof ('3' - '3'));
console.log("'3' / '3': ", '3' / '3', "\t type: ", typeof ('3' / '3'));
console.log("'3' / 3: ", '3' / 3, "\t type: ", typeof ('3' / 3));
console.log("3 / '3': ", 3 / '3', "\t type: ", typeof (3 / '3'));
console.log("3 * '3': ", 3 * '3', "\t type: ", typeof (3 * '3'));
console.log("'hey' * 3: ", 'hey' * 3, "\t type: ", typeof ('hey' * 3));
console.log("'hey' * 'hello': ", 'hey' * "hello", "\t type: ", typeof ('hey' * "hello"));
console.log();


// float
console.log();
console.log("'3' + 3.26: ", '3' + 3.26);
console.log("3.26 + 3: ", 3.26 + '3');
console.log("'3' - 3.26: ", '3' - 3.26);
console.log("3 * '3.26': ", 3 * '3.26');
console.log("3.26 + '3': ", 3.26 + '3');
console.log("'3.26' % 3: ", '3.26' % 3);
console.log();


// boolean
console.log();
console.log("true + 'true': ", true + 'true');
console.log("true + true: ", true + true);
console.log("23 + true: ", 23 + true);
console.log("23.61 + true: ", 23.61 + true);
console.log("23.61 - true: ", 23.61 - true);
console.log("'hey' - true: ", "hey" - true);
console.log("true + 'hey': ", true + "hey");
console.log();



// null and undefined
console.log();
console.log("true + null: ", true + null);
console.log("true + undefined: ", true + undefined);

console.log("4 - null: ", 4 - null);
console.log("4 - undefined: ", 4 - undefined);

console.log("'4' * null: ", '4' * null);
console.log("'4' * undefined: ", '4' * undefined);

console.log("undefined + null: ", undefined + null);
console.log("null - undefined: ", null - undefined);
console.log();



console.log()
console.log("---------- Explicit Type Conversion ------------");
console.log();

// convert into number 
console.log("Number('5'): ", Number('5'));
console.log("Number('hey'): ", Number('hey'));
console.log("Number('10.5'): ", Number('10.5'));
console.log("Number('true'): ", Number('true'));
console.log("Number(true): ", Number(true));
console.log("Number(false): ", Number(false));
console.log("Number(undefined): ", Number(undefined));
console.log("Number(null): ", Number(null));
console.log("Number(''): ", Number(''));
console.log();


// convert into boolean 
console.log();
console.log("Boolean(null): ", Boolean(null));
console.log("Boolean(undefined): ", Boolean(undefined));
console.log("Boolean(0): ", Boolean(0));
console.log("Boolean(1): ", Boolean(1));
console.log("Boolean('1'): ", Boolean('1'));
console.log("Boolean('0'): ", Boolean('0'));
console.log("Boolean('hey'): ", Boolean('hey'));
console.log("Boolean('53.67'): ", Boolean('53.67'));
console.log("Boolean(53.67): ", Boolean(53.67));
console.log();


// parseInt()
console.log();
console.log("parseInt(null): ", parseInt(null));
console.log("parseInt('null'): ", parseInt('null'));
console.log("parseInt('hey'): ", parseInt('hey'));
console.log("parseInt('134.78'): ", parseInt('134.78'));
console.log("parseInt('134.78', 2): ", parseInt('134.78', 2));
console.log("parseInt('134.78', 8): ", parseInt('134.78', 8));
console.log("parseInt(true): ", parseInt(true));
console.log();



// parseFloat()
console.log();
console.log("parseFloat(null): ", parseFloat(null));
console.log("parseFloat('null'): ", parseFloat('null'));
console.log("parseFloat('hey'): ", parseFloat('hey'));
console.log("parseFloat('134.78'): ", parseFloat('134.78'));
console.log("parseFloat('132'): ", parseFloat('132'));
console.log("parseFloat(true): ", parseFloat(true));
console.log();


// toString()
console.log();
// console.log("(null).toString(): ", (null).toString());   // it cause error of: cannot read property of null
console.log("('null').toString(): ", ("null").toString());
console.log("('undefined').toString(): ", ("undefined").toString());
console.log("(312).toString(): ", (312).toString());
console.log("(true).toString(): ", (true).toString());
console.log("(10.813).toString(): ", (10.813).toString());
console.log("({name: 'nawaraj', age:23}).toString(): ", ({ name: 'nawaraj', age: 23 }).toString());
console.log()


// check equality
console.log();
console.log("----------- equality check ----------")
var a = 23;
var b = 24;
var c = "23";

console.log();
console.log("value of 'a': ", a);
console.log("value of 'b': ", b);
console.log("value of 'c': ", c);
console.log();

console.log("a == b: ", a == b);
console.log("a == c: ", a == c);
console.log("a === c: ", a === c);

console.log();
console.log("null == undefined: ", null == undefined);
console.log("null === undefined: ", null === undefined);
console.log("false == undefined: ", false == undefined);
console.log("false === undefined: ", false === undefined);

console.log();
console.log("false == null: ", false == null);
console.log("false === null: ", false === null);
console.log("null == false: ", null == false);
console.log()







