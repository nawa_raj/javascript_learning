// hoisting is the concept that we use in javascript
// it means we can access variabel before its decleration, or initialization.

/*
    // javascript code...
    var a = 45;
    console.log(a, ab)

    var ab = 689;
    ...


    // in javascript, javascript compiler compile above file as below:

    var a;
    var ab;

    a = 45;

    console.log(a, ab)      // 45, undefined 

    ab = 689;
    ...
    
    // it declare variable first at the top of program and do rest of the code as like we define 
*/


// variable decleration
var b;
let c;

// we cannot declare const variable, we need to assing value along with
// const t;             // it throw error 
const t = "lets see"    // it is ok



console.log("\n--------- variable hoisting -------------\n")
console.log("value of 'b': ", b);
console.log("value of 'c': ", c);
console.log()

// value assignment in variable:
b = 23;
c = "its work"

console.log("after value assignment 'b': ", b);
console.log("after value assignment 'c': ", c);
console.log()




// we can refer first and after that we can declare variable
console.log("\n----------- var Variable can be refer before it can be declared -----------------\n")

console.log("value of 'e': ", e)

// we cannot access variable before initialization with let keyword
// console.log("value of 'd': ", d)

// we cannot access variable before initialization or value with const keyword
// console.log("value of 'ab': ", ab)
console.log()

var e;
let d;
const ab = "lets try later initialization"



// we can also see in the function
var cv = "this is a initial message"

function variableHoisting() {

    console.log("value of 'cv': ", cv);

    var cv = "let's it can be access before assignment"

    // if we decleared with let it give Reference Error...
    // let cv = "let's it can be access before assignment"
}

console.log("\n-------------- variable hoisting in function ---------------\n")
variableHoisting();
console.log("variable 'cv': ", cv);
console.log()





// function hoisting
console.log("\n--------- function hoisting --------------\n")

/* Function declaration */
foo();                  // it run perfectly


function foo() {
    console.log('bar');
}

/* ----------- Function expression are not hoisted in javascript ---------------- */

// baz(); // TypeError: baz is not a function

var baz = function () {
    console.log('bar2');
};

baz();      // it run perfectly 
console.log()




// we can refer variable before declare it 
console.log("\n------ variable value can be get before decleration ---------");

// variable initialize or assignment...
we = "we are here"

console.log("the value of 'we': ", we);
console.log();

// variable decleration here
var we;