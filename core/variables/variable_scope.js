//  in javascipt we can define global variable inside function and access from outside the function

/* 
    in javascript if we didn't give variable decleration keyword 
    that become global variable and accessable anywhere 

*/

function display() {

    // global variable
    j = "this is a global variable";

    var a = "this is a local variable with keyword var";
    let b = "this is a local variable with keyword let";
    const c = "this is a local variable with keyword let";

    console.log(`\n${a}\n${b}\n${c}`)
}


console.log("\n---------- make globla variable from inside function ------------\n")
display()
console.log("accessing 'j' from outside function:\n", j)
console.log()


// variable block asope 
function blockScope() {
    let a = 2;

    if (a < 5) {
        var c = 20;
        const b = 10;
        console.log("if block variable 'b': ", b)
        console.log("function scope variable 'a': ", a)
    }

    /* 
        it throw error because b is decleared inside the if block with const keyword 
        that's why it is not accessible from outside if block 
    */
    // console.log("we are trying to access if block variable 'b': ", b)

    // if variable declared with const and let keyword we cannot access from outside of block statement
    // variable "c" has global scope so that we can access from outside of block scope 
    console.log("accessing 'c' value from outside if block which is declared with var keyword: ", c)
}

console.log("\n----------- block scope variable ----------------\n")
blockScope()
console.log()



// same name variable in different variable scope sector
function sameNameVariable() {
    let a = 2;

    // var a = 20; // it cannot be defined with same variable name again
    var b = 20;

    console.log("with var keyword 'b': ", b)

    if (a < 5) {
        var b = 7598;
        let a = 10;
        console.log("value of 'a' with same name in if block: ", a)
    }

    // here we get value of b = 7598 because it's global scope it change the value of b from inside of block
    console.log(b)

    // it cause error because let cannot be re decleared 
    // let a = 20; 

    var b = 30 // we can re declare like this because var keyword allow to declare variable
    console.log("re declare variable 'b' with var keyword: ", b);
}

console.log("\n---------- same name variable decleration --------------\n")
sameNameVariable()
console.log()



// function scope of variable:
function variableFunctionScope() {
    const a = 30;

    function inner() {
        // if we declared as like this it means it is a global variable 
        // b = 23

        let b = 23;         // it means local scope i.e. it is available only inside inner function

        console.log("inner function local variable 'b': ", b)
        console.log("accessing parent function variable 'a' from inner function: ", a)
    }

    inner();

    // inner function variable 'b' cannot access from parent function 
    // console.log("trying to access inner function variable 'b' from parent function: ", b);
}

console.log("\n-------------- variable function scope -----------------\n")
variableFunctionScope()
console.log()



