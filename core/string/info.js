/*
    The String object is used to represent and manipulate a sequence of characters.    
    
    We can declare or assing sting value to variable with two method:
    1. By Using Constructor Method
    2. By using String Literals.

*/


// declaring string using constructor function of String object
console.log("\n---------- Declaring String Using Constructor Function -----------\n");

// it create string object
let str1 = new String("Hello, Nawaraj Jaishi");

// it create string Primitive
let str2 = String("Declering string using String function");

console.log(str1);
console.log(str2);
console.log();





// declaring string using string literals 
console.log("\n------------ String Decleration Using String Literals -----------------\n");

str1 = "this is re-defined using string literals";
str2 = 'String Declare with single quote';
let str3 = `declare string using back tick`;

console.log(str1);
console.log(str2);
console.log(str3);
console.log();