/*
    we use the escape sequence to add some extra things to string visulation
*/


// working with string escape sequence:
const hey = 34;

console.log("\n---------- Escape Sequence in String -------------\n");
console.log("use of 'null character': ", "hey man\0 I'm going to america", "\n");
console.log("use of 'horizantal tag': ", "hey man \t I'm going to america");
// console.log("use of 'vertical tag': ", "hey man \vI'm going to america");
console.log("use of 'new line': ", "hey man \n I'm going to america");
console.log("use of 'single quote': ", "hey man I'm \'going to america\'");
console.log("use of 'double quote': ", "hey man I'm \"going to america\"");
// console.log("use of 'back space': ", "hey man I'm \b going to america");
// console.log("use of 'back space': ", "hey man I'm \r going to america", hey);
console.log();