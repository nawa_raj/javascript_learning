/*
    the toUpperCase() and toLowerCase() method in javascript help to make string upper and lower as per 
    requirement 

    Syntax:
        toLowerCase()
        toUpperCase()

    Return Value:
    new string after specified operation 
*/

console.log("\n----------- toLowerCase() and toUpperCase() mrthods --------------\n");

const sentence = 'The Quick Brown Fox jumps over the lazy Dog.';

console.log("orginal string: ", sentence)
console.log("sentence.toLowerCase(): ", sentence.toLowerCase(), "\n");

console.log("orginal string: ", sentence)
console.log("sentence.toUpperCase(): ", sentence.toUpperCase(), "\n");
