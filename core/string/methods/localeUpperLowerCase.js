/*
    the toLocaleUpperCase() and toLocalLowerCase() method returns the calling string value 
    converted to upper and lower case, according to any locale-specific case mappings. 

    Syntax:
        toLocaleLowerCase()
        toLocaleLowerCase(locale)
        toLocaleLowerCase([locale1, locale2, ...])

        toLocaleUpperCase()
        toLocaleUpperCase(locale)
        toLocaleUpperCase([locale1, locale2, ...])

    Return Value:
    new string after specified operation 
*/

console.log("\n----------- toLocaleLowerCase() and toLocaleUpperCase() mrthods --------------\n");

const city = 'istanbul';

console.log("orginal string: ", city)
console.log(city.toLocaleUpperCase('en-US'));           // expected output: "ISTANBUL"
console.log(city.toLocaleUpperCase('ne-NP'));           // expected output: "istanbul"



let locales = ['tr', 'TR', 'tr-TR', 'tr-u-co-search', 'tr-x-turkish'];
console.log('\u0130'.toLocaleLowerCase(locales) === 'i');                   // true



const dotted = 'İstanbul';
console.log("\norginal string: ", dotted);
console.log(`EN-US: ${dotted.toLocaleLowerCase('en-US')}`);
// expected output: "i̇stanbul"

console.log(`TR: ${dotted.toLocaleLowerCase('tr')}`);
// expected output: "istanbul"

console.log();