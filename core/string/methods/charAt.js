/*
    The String object's charAt() method returns a new string consisting of the single UTF-16 code 
    unit located at the specified offset into the string.

    note: it doesn't take negative index if provided it return empty string.

    Syntax
        string.charAt(index);

    Return value
    A string representing the character (exactly one UTF-16 code unit) at the specified index. 
    If index is out of range, charAt() returns an empty string.

    it doesnot change the orginal string.

*/



console.log("\n----------- String.prototype.charAt() ------------------\n");

const sentence = 'The quick brown fox jumps over the lazy dog.';
console.log("Orginal String: ", sentence, "\n");

console.log(`The character at index ${4} is: ${sentence.charAt(4)}`);
console.log(`The character at index ${16} is: ${sentence.charAt(16)}`);
console.log(`The character at index ${90} is: ${sentence.charAt(90)}`);

// it not take the negative index as parameter: that return empty string
console.log(`The character at index ${-5} is: ${sentence.charAt(-5)}`);
console.log(`The character at index ${-21} is: ${sentence.charAt(-21)}`);


console.log("\nOrginal String: ", sentence);
console.log();
