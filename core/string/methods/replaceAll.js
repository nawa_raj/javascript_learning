/*
    The replaceAll() method returns a new string with all matches of a pattern replaced by a replacement.
    The pattern can be a string or a RegExp, and the replacement can be a string or a function to be 
    called for each match.

    The original string is left unchanged.
    A RegExp without the global ("g") flag will throw a TypeError: "replaceAll must be called with a global RegExp".


    Syntax:
        replaceAll(regexp, newSubstr)
        replaceAll(regexp, replacerFunction)
        replaceAll(substr, newSubstr)
        replaceAll(substr, replacerFunction)

    Return Value:
    New string with all matches replaced value.
*/

console.log("\n--------------- String.prototype.replaceAll() with string --------------\n");

let p = 'The quick brown fox jumps over the lazy dog. If the dog reacted, was it really lazy?';

console.log("orginal string: ", p);
console.log("p.replaceAll('dog', 'monkey'): ", p.replaceAll('dog', 'monkey'));
console.log();


// global flag required when calling replaceAll with regex
console.log("\n--------------- String.prototype.replaceAll() with regex --------------\n");

let regex = /dog/gi;
console.log("regex: ", regex);
console.log("p.replaceAll(regex, '-jambon-'): ", p.replaceAll(regex, '-jambon-'));
console.log();


