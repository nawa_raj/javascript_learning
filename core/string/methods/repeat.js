/*
    The repeat() method constructs and returns a new string which contains the specified number
    of copies of the string on which it was called, concatenated together.

    Syntax:
        repeat(count)

    Return Value:
    A new string containing the specified number of copies of the given string.
*/

console.log("\n------------ String.prototype.repeat() method --------------\n");
const chorus = 'Because I\'m happy. ';

console.log("orginal string: ", chorus, "\n");
console.log("chorus.repeat(10): \n", chorus.repeat(10));
console.log();

