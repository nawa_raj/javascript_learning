/*
    pad refer to the padding method in javascript string object.
    there are two type of padding method available in javascript.

    1. padEnd():
        The padEnd() method pads the current string with a given string (repeated, if needed) so 
        that the resulting string reaches a given length. The padding is applied from the end of 
        the current string.

        Syntax:
            padEnd(targetLength)
            padEnd(targetLength, padString)
        
        Return value:
        A new String of the specified targetLength with the padString applied at the end of the current str.
    
    
    2. padStart():
        The padStart() method pads the current string with another string (multiple times, if needed)
        until the resulting string reaches the given length. The padding is applied from the start of
        the current string.

        Syntax:
        padStart(targetLength)
        padStart(targetLength, padString)

    Return value:
        A new String of the specified targetLength with the padString applied at the start of the current str.
*/



// String.prototype.padStart() method:
console.log("\n---------- String.prototype.padStart() method -------------\n");

let str = '5';
let str1 = "args";
let len = str.length;

console.log("orginal string: ", str);
console.log("str.padStart(len + 2, '0'): ", str.padStart(len + 2, '0'), "\n");

console.log("orginal string: ", str1);
console.log("str.padStart(str1.length + 3, '.'): ", str1.padStart(str1.length + 3, '.'), "\n");

str1 = "World";
let appendStr = "hello ";

console.log("orginal string: ", str1);
console.log("orginal appendable string: ", appendStr);
console.log("str.padStart(str1.length + appendStr.length, appendStr): ", str1.padStart(str1.length + appendStr.length, appendStr), "\n");

console.log("str.padStart(str1.length + appendStr.length*2, appendStr): ", str1.padStart(str1.length + (appendStr.length * 2), appendStr), "\n");
console.log();





// String.prototype.padEnd() method:
console.log("\n---------- String.prototype.padEnd() method -------------\n");
str = '5';
str1 = "args";
len = str.length;

console.log("orginal string: ", str);
console.log("str.padEnd(len + 2, '0'): ", str.padEnd(len + 2, '0'), "\n");

console.log("orginal string: ", str1);
console.log("str1.padEnd(str1.length + 3, '.'): ", str1.padEnd(str1.length + 3, '.'), "\n");
// console.log(str1)

str1 = "hello";
appendStr = " world";

console.log("orginal string: ", str1);
console.log("orginal appendable string: ", appendStr);
console.log("str1.padEnd(str1.length + appendStr.length, appendStr): ", str1.padEnd(str1.length + appendStr.length, appendStr), "\n");
console.log("str1.padEnd(str1.length + appendStr.length*2, appendStr): ", str1.padEnd(str1.length + (appendStr.length * 2), appendStr), "\n");
console.log("str1.padEnd(10): ", `"${str1.padEnd(10)}"`, "\n");
console.log();

