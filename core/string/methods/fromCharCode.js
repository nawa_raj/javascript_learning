/*
    The static String.fromCharCode() method returns a string created from the specified 
    sequence of UTF-16 code units.

    Syntax:
        String.fromCharCode(num1, num2, ..., numN)

    Return value
    A string of length N consisting of the N specified UTF-16 code units.
*/

console.log("\n----------- String.fromCharCode() ---------------\n");

console.log("String.fromCharCode(189, 43, 190, 65535): ", String.fromCharCode(189, 43, 190, 65535));      // expected output: "½+¾="
console.log("String.fromCharCode(65, 66, 67): ", String.fromCharCode(65, 66, 67));                        // returns "ABC"
console.log("String.fromCharCode(0x2014):", String.fromCharCode(0x2014));               // returns "—"
console.log("String.fromCharCode(0xD83C, 0xDF03): ", String.fromCharCode(0xD83C, 0xDF03));
console.log("String.fromCharCode(42): ", String.fromCharCode(42));
console.log("String.fromCharCode(9733): ", String.fromCharCode(9733));
console.log();