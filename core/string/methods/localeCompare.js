/*
    The localeCompare() method returns a number indicating whether a reference string comes before,
    or after, or is the same as the given string in sort order.


    Syntax
        localeCompare(compareString)
        localeCompare(compareString, locales)
        localeCompare(compareString, locales, options)


    Return value:
    A negative number if referenceStr occurs before compareString; positive if the referenceStr
    occurs after compareString; 0 if they are equivalent.

*/


const referenceStr = 'réservé'; // with accents, lowercase
const compareString = 'RESERVE'; // no accents, uppercase

console.log("\n---------- String.prototype.localeCompare() -------------\n");

console.log("reference string: ", referenceStr);
console.log("comparing string: ", compareString, "\n");

console.log(referenceStr.localeCompare(compareString));     // expected output: 1
console.log(referenceStr.localeCompare(compareString, "en", { sensitivity: 'base' }));      // expected output: 0
console.log(referenceStr.localeCompare(compareString, 'en'));
console.log();

console.log('ä'.localeCompare('a', 'de', { sensitivity: 'base' })); // 0
console.log("2".localeCompare("10", undefined, { numeric: true })); // -1
console.log('ä'.localeCompare('a', 'sv', { sensitivity: 'base' })); // a positive value
console.log();


