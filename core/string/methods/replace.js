/*
    The replace() method returns a new string with some or all matches of a pattern replaced by a 
    replacement. The pattern can be a string or a RegExp, and the replacement can be a string or a 
    function to be called for each match. If pattern is a string, only the first occurrence will 
    be replaced.

    It replace first occurance in string.
    The original string is left unchanged.

    Syntax
        replace(regexp, newSubstr)
        replace(regexp, replacerFunction)

        replace(substr, newSubstr)
        replace(substr, replacerFunction)


    Return value
    A new string, with some or all matches of a pattern replaced by a replacement.
*/


console.log("\n------------- String.prototype.replace() method replace with string --------------\n");

let p = 'The quick brown fox jumps over the lazy dog. If the dog reacted, was it really lazy?';

console.log("orginal string: ", p);
console.log("p.replace('dog', 'monkey'): ", p.replace('dog', 'monkey'));
console.log();



console.log("\n------------- String.prototype.replace() method replace with regex --------------\n");


console.log("orginal string: ", p);
console.log("p.replace(/dog/gi, 'Monkey'): ", p.replace(/(dog)/gi, 'Monkey'));
console.log("p.replace(/dog/i, 'Monkey'): ", p.replace(/(dog)/i, 'Monkey'));

let reg = /(dog)/i;
console.log(reg.exec(p));
console.log();

console.log("orginal string: ", p);
console.log();



console.log("\n------------- String.prototype.replace() method replace with function --------------\n");

function replacer(match, p1, p2, p3) {
    console.log(arguments);
    // p1 is nondigits, p2 digits, and p3 non-alphanumerics
    return [p1, p2, p3].join(' - ');
}
let newString = 'abc12345#$*%'.replace(/([^\d]*)(\d*)([^\w]*)/, replacer);
console.log(newString);  // abc - 12345 - #$*%
console.log();