/*
    The charCodeAt() method returns an integer between 0 and 65535 representing the UTF-16 
    code unit at the given index.

    this method doesnot take negative integer. if provided it return undefined.

    Syntax
    string.charCodeAt(index)

    Return value
    A number representing the UTF-16 code unit value of the character at the given index.
    If index is out of range, charCodeAt() returns NaN.
*/


console.log("\n----------- String.prototype.charCodeAt() -------------\n");
const sentence = 'The quick brown fox jumps over the lazy dog.';

console.log("orginal string: ", sentence);
console.log(`The character code ${sentence.charCodeAt(4)} is equal to ${sentence.charAt(4)}`);
console.log(`The character code ${sentence.charCodeAt(-5)} is equal to ${sentence.charAt(-5)}`);
console.log();



// uppercase letter and lowercase letter code is different.
let str1 = "abc";
let str2 = "ABC";

console.log("\n-------------- Upper and Lower Case letter code is different ----------------\n");
console.log("orginal string 1: ", str1);
console.log("orginal string 2: ", str2);

console.log("str1.charCodeAt(1): ", str1.charCodeAt(1))
console.log("str2.charCodeAt(1): ", str2.charCodeAt(1))
console.log("str1.charCodeAt(1) === str2.charCodeAt(1): ", str1.charCodeAt(1) === str2.charCodeAt(1));
console.log();