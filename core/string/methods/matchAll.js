/*
    The matchAll() method returns an iterator of all results matching a string against a 
    regular expression, including capturing groups.

    Syntax:
        matchAll(regexp)


    Return Value:
    An iterator (which is not a restartable iterable) of matches.
*/


console.log("\n------------- matchAll() method -------------\n");

let regexp = /t(e)(st(\d?))/g;
let str = 'test1test2 test3 test';

let matchAll = str.matchAll(regexp);

console.log("orginal string: ", str, "\n");
console.log("regex: ", regexp);
console.log("str.matchAll(regexp): ", matchAll);
console.log("make array from iterator: ", [...matchAll]);

// const matchAllArr = [...str.matchAll(regexp)];
// console.log("matchAllArr = [...matchAll]: ", matchAllArr);

console.log("\nWe use Array.from() static method to get the all matches in the form of an array:");

console.log(Array.from(str.matchAll(regexp), x => x[0]));
console.log();


// console.log("\n------regexp.exec(str)------------\n");
// console.log(regexp.exec(str));
// console.log();


// const range = (start, step, stop) => Array.from({ length: (stop - start) / step + 1 }, (_, i) => {
//     console.log(i);
//     return start + (i * step);
// });

// console.log(range(2, 2, 10));

