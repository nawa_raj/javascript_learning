/*
    The includes() method performs a case-sensitive search to determine whether one string may 
    be found within another string, returning true or false as appropriate.

    Syntax:
        string.includes(searchString, position)

    Return value:
    true if the search string is found anywhere within the given string; otherwise, false if not.
*/

console.log("\n--------- String.prototype.includes() ----------------\n");
const sentence = 'The quick brown fox jumps over the lazy dog.';

console.log("orginal string: ", sentence, "\n");
console.log("sentence.includes('fox'): ", sentence.includes('fox'));
console.log("sentence.includes('word'): ", sentence.includes('word'));


// includes method search string with case sensetiveness rule.
console.log("\n----- includes() method is case sensetive ------\n");
console.log("sentence.includes('Fox'): ", sentence.includes('Fox'));



console.log();
console.log("\n--------- String.prototype.includes() example----------------\n");
const str = 'To be, or not to be, that is the question.'

console.log("orginal string: ", str, "\n");
console.log("str.includes('To be'): ", str.includes('To be'))                                   // true
console.log("str.includes('question', ): ", str.includes('question'))                           // true

console.log(`str.includes('question', ${str.length - 1}): `,
    str.includes('question', str.length - 1))                                                   // false

console.log("str.includes('nonexistent'): ", str.includes('nonexistent'))                       // false
console.log("str.includes('To be', 1): ", str.includes('To be', 1))                             // false
console.log("str.includes('TO BE'): ", str.includes('TO BE'))                                   // false
console.log("str.includes(''): ", str.includes(''))                                             // true
console.log();




