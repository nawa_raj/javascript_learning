/*
    The length property of a String object contains the length of the string, in UTF-16 code units.
    length is a read-only data property of string instances.

    Syntax:
        string.length
    
    Return Value:
    length of string.
    
    it doesnot modefied the orginal string.
*/


const str = 'Life, the universe and everything. Answer:';

console.log("\n-------------- string length property ---------------\n");
console.log("orginal string: ", str);
console.log("str.length: ", str.length);
console.log();