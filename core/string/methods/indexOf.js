/*
    The indexOf() method, given one argument: a substring to search for, searches the entire 
    calling string, and returns the index of the first occurrence of the specified substring.
    Given a second argument: a number, the method returns the first occurrence of the specified 
    substring at an index greater than or equal to the specified number.

    Syntax
        string.indexOf(searchString)
        string.indexOf(searchString, position)


    Return value
    The index of the first occurrence of searchString found, or -1 if not found.
*/

console.log("\n------------- String.prototype.indexOf() ------------\n");
const paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';

const searchTerm = 'dog';
const indexOfFirst = paragraph.indexOf(searchTerm);

console.log("orginal string: ", paragraph, "\n");
console.log(`paragraph.indexOf(${searchTerm})`, indexOfFirst);
console.log(`paragraph.indexOf(${searchTerm}, ${indexOfFirst + 1})`, paragraph.indexOf(searchTerm, (indexOfFirst + 1)));
console.log();


// it is search with strictly equality check "==="
const indexOfJump = paragraph.indexOf("jumps")
console.log(`paragraph.indexOf("jumps")`, indexOfJump);
console.log(`paragraph.indexOf("jumps", ${indexOfJump + 2})`, paragraph.indexOf("jumps", indexOfJump + 2));
console.log();


// it is search with strictly equality check "==="
console.log("\n--------- it implement strict equality check rule -------------")
console.log(`paragraph.indexOf("jumps")`, paragraph.indexOf("jumps"));
console.log(`paragraph.indexOf("Jumps")`, paragraph.indexOf("Jumps"));
console.log();