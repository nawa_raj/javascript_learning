/*
    The valueOf() method returns the primitive value of a String object.

    Syntax:
        valueOf()

    Return Value:
    A string representing the primitive value of a given String object.
*/



console.log("\n------------ String.prototype.valueOf() method --------------\n");

var x = new String('Hello world');
console.log("string object: ", x);
console.log("x.valueOf(): ", x.valueOf());          // Displays 'Hello world'
console.log();