/*
    The split() method divides a String into an ordered list of substrings, 
    puts these substrings into an array, and returns the array. The division is done by searching 
    for a pattern; where the pattern is provided as the first parameter in the method's call.

    Syntax:
        split()
        split(separator)
        split(separator, limit)

    Return Value:
    New array with all the splited string.
*/

console.log("\n-------- String.prototype.split() method -----------\n");

const str = 'The quick brown fox jumps over the lazy dog.';
console.log("orginal string: ", str);

const words = str.split(' ');
console.log("str.split(' '): ", words);

const chars = str.split('');
console.log("str.split(''): ", chars);

const strCopy = str.split();
console.log("str.split(): ", strCopy);
console.log();


// Removing spaces from a string
console.log("\n----------- Removing spaces from a string --------------\n");

const names = 'Harry Trump ;Fred Barney; Helen Rigby ; Bill Abel ;Chris Hand '
const re = /\s*(?:;|$)\s*/

console.log("orginal string: ", names);
console.log("regex: ", re);

const nameList = names.split(re)

console.log("\nnames.split(re): ", nameList)
console.log();