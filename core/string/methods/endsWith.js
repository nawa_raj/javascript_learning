/*
    The endsWith() method determines whether a string ends with the characters of a specified string, 
    returning true or false as appropriate.

    Syntax:
        string.endsWith(searchString, length);

    Return value
    true if the given characters are found at the end of the string; otherwise, false.
*/

console.log("\n-------------- String.prototype.endsWith() ------------------\n");

const str1 = 'Cats are the best!';
const str2 = 'Is this a question';

console.log("orginal string1: ", str1);
console.log("orginal string2: ", str2, "\n");

console.log("str1.endsWith('best', 17): ", str1.endsWith('best', 17));          // expected output: true
console.log("str1.endsWith('!'): ", str1.endsWith('!'));                        // expected output: true
console.log("str2.endsWith('?'): ", str2.endsWith('?'));                        // expected output: false
console.log();
