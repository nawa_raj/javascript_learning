/*
    The codePointAt() method returns a non-negative integer that is the Unicode code point 
    value at the given position.
    
    this method doesnot take negative integer. if provided it return undefined.

    Syntax        
        string.codePointAt(pos)

    Return value
    A decimal number representing the code point value of the character at the given pos. 
    if index not found it return 'undefined'

    
*/


console.log("\n---------------- String.prototype.codePointAt() ----------------\n");
const icons = '☃★♲';

console.log("orginal string: ", icons);

console.log("icons.codePointAt(0): ", icons.codePointAt(0));
console.log("icons.codePointAt(1): ", icons.codePointAt(1));
console.log("icons.codePointAt(2): ", icons.codePointAt(2));

console.log("\nnow using charCodeAt\n");
console.log("charCodeAt(0): ", icons.charCodeAt(0));
console.log("charCodeAt(1): ", icons.charCodeAt(1));
console.log("charCodeAt(2): ", icons.charCodeAt(2));
console.log();