/*
    The substring() method returns the part of the string between the start and end indexes,
    or to the end of the string.

    Differences between substring() and slice():
        The substring() method swaps its two arguments if indexStart is greater than indexEnd.
        substring doesnot support negative index.

    
    Syntax:
        substring(indexStart)
        substring(indexStart, indexEnd)

    Return value
    A new string containing the specified part of the given string.
*/




console.log("\n------------- String.prototype.substring() ---------------\n");

const str = 'Mozilla';
console.log("orginal string: ", str, "\n");
console.log("str.substring(1, 3): ", str.substring(1, 3));       // expected output: "oz"
console.log("str.substring(2): ", str.substring(2));            // expected output: "zilla"
console.log();

//------------ Differences between substring() and slice() ----------------------------
// substring swap the arguments if start index is greater than end endex
console.log("\n---------- substring swap the arguments if start index is greater than end endex -----------\n");

let text = 'Mozilla'
console.log("orginal string: ", text, "\n");
console.log("text.substring(5, 2): ", text.substring(5, 2))         // => "zil"
console.log("text.slice(5, 2): ", text.slice(5, 2))                 // => ""
console.log();


//If either or both of the arguments are negative or NaN, the substring() method treats them as if they were 0.
console.log("\n-------------- substring() treated NaN value as 0 ------------------\n");
console.log("text.substring(-5, 2): ", text.substring(-5, 2))       // => "Mo"
console.log("text.substring(-5, -2): ", text.substring(-5, -2))     // => ""
console.log();


// slice() also treats NaN arguments as 0, but when it is given negative values it counts backwards from the end of the string to find the indexes.
console.log("\n-------------- slice() also treated NaN value as 0 but if negative it count from backward ------------------\n");

console.log("text.slice(-5, 2): ", text.slice(-5, 2))           // => ""
console.log("text.slice(-5, -2): ", text.slice(-5, -2))         // => "zil"
console.log();

