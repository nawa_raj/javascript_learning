/*
    The lastIndexOf() method, given one argument: a substring to search for, searches the entire calling 
    string, and returns the index of the last occurrence of the specified substring. 
    Given a second argument: a number, the method returns the last occurrence of the specified 
    substring at an index less than or equal to the specified number.

    Syntax:
        lastIndexOf(searchString, position)

    
    Return value
    The index of the last occurrence of searchString found, or -1 if not found.

*/

console.log("\n------------- String.prototype.lastIndexOf() ------------\n");

const paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';
const paragraph1 = "This dog is really dangerous who fox. who attempt to attack dog's owner. thats why mr. brown really love his dog?";
const searchTerm = 'dog';

console.log("orginal string: ", paragraph);
console.log("orginal string: ", paragraph1, "\n");

console.log("from first paragraph: ");
console.log(`paragraph.lastIndexOf(${searchTerm}): `, paragraph.lastIndexOf(searchTerm));

console.log("\n\nfrom second paragraph:");
console.log(`paragraph1.lastIndexOf("really"): `, paragraph1.lastIndexOf("really"));
console.log(`paragraph1.lastIndexOf("world"): `, paragraph1.lastIndexOf("world"));


console.log("\n\while we use indexOf() Method in second paragraph:\n", "it matches the first occurance of search term");
console.log(`paragraph1.lastIndexOf("really"): `, paragraph1.indexOf("really"));
console.log(`paragraph1.lastIndexOf("world"): `, paragraph1.lastIndexOf("world"));
console.log()

