/*
    The static String.fromCodePoint() method returns a string created by using the specified 
    sequence of code points.

    Syntax:
        String.fromCodePoint(num1, num2, ..., numN)

    Return value
    A string created by using the specified sequence of code points.
*/


console.log("\n---------------- String.fromCodePoint() ------------\n");
console.log("String.fromCodePoint(9731, 9733, 9842, 0x2F804): ", String.fromCodePoint(9731, 9733, 9842, 0x2F804));   // expected output: "☃★♲你"
console.log("String.fromCodePoint(42): ", String.fromCodePoint(42));
console.log("String.fromCodePoint(9733): ", String.fromCodePoint(9733));
console.log("String.fromCodePoint(0x2F804): ", String.fromCodePoint(0x2F804));
console.log();