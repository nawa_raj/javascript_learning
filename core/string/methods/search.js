/*
    The search() method executes a search for a match between a regular expression and this String object.

    Syntax:
        search(regexp)


    Return Value:
    The index of the first match between the regular expression and the given string,
    or -1 if no match was found.
*/


console.log("\n---------- String.prototype.search() method --------------\n");

let paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';

// any character that is not a word character or whitespace
let regex = /[^\w\s]/g;

console.log("orginal string: ", paragraph);
console.log("regex: ", regex);
console.log("paragraph.search(regex): ", paragraph.search(regex));
console.log();


// 
let str = "hey JudE"
let re = /[A-Z]/g;
let reDot = /[.]/g;
console.log("orginal string: ", str);
console.log("regex: ", re);
console.log("str.search(re): ", str.search(re))    // returns 4, which is the index of the first capital letter "J"

console.log("\nreg: ", reDot);
console.log("str.search(reDot): ", str.search(reDot)) // returns -1 cannot find '.' dot punctuation
console.log();
