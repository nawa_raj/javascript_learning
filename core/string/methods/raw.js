/*
    The static String.raw() method is a tag function of template literals. 
    This is similar to the r prefix in Python, or the @ prefix in C# for string literals. 
    It's used to get the raw string form of template literals, that is, substitutions (e.g. ${foo})
    are processed, but escapes (e.g. \n) are not.

    Syntax:
        String.raw(callSite, ...substitutions)
        String.raw`templateString`

    Return value
    The raw string form of a given template literal.
*/

console.log("\n------------ String.raw() static method ---------------\n");

const filePath = String.raw`C:\Development\profile\aboutme.html`;
console.log("raw file path: ", filePath);

console.log();