/*
    The match() method retrieves the result of matching a string against a regular expression.

    Syntax:
        match(regexp)
    
    
    Return value
    An Array whose contents depend on the presence or absence of the global (g) flag, 
    or null if no matches are found.
    
*/

console.log("\n--------------- get user info from raw data ----------------\n");

let str = 'ram sharan sharma 29 9856798 kolti bajura netra bahadur swaud 79 79087980 jhalari mahendranagar';

let re = /\w+[^ _-]/gi;
let found = str.match(re);

console.log("orginal string: ", str);
console.log("search regex: ", re, '\n');

console.log("match data: ", found);


let objArry = [];

for (let i = 0; i < found.length; i += 7) {
    let obj = {};

    if (i < 7) {
        obj['name'] = `${found[0]} ${found[1]} ${found[2]}`;
        obj['age'] = found[3];
        obj['contact'] = found[4];
        obj['address'] = `${found[5]} ${found[6]}`;

        objArry = [...objArry, obj];

    } else {
        obj['name'] = `${found[0 + 7]} ${found[1 + 7]} ${found[2 + 7]}`;
        obj['age'] = found[3 + 7];
        obj['contact'] = found[4 + 7];
        obj['address'] = `${found[5 + 7]} ${found[6 + 7]}`;

        objArry = [...objArry, obj];
    }

};

console.log("\nuser info: ", objArry);
console.log();




// Using named capturing groups
console.log("\n----------- Using named capturing groups -----------------\n");
let paragraph = 'The quick brown fox jumps over the lazy dog cat. It barked.';

let reg = /(?<animal>fox|cat)/g;
found = paragraph.match(reg);

console.log("orginal string: ", paragraph);
console.log("regex: ", reg, "\n");
console.log("matches: ", found.groups);                  // {animal: "fox"}
console.log();





// Using match() with no parameter
console.log("\n----------- Using match() with no parameter -------------\n");

str = "Nothing will come of nothing.";

console.log("orginal string: ", str);
console.log("str.match(): ", str.match());
console.log();




// A non-RegExp object as the parameter
console.log("\n------------ A non-RegExp object as the parameter ---------------\n");
let str1 = "NaN means not a number. Infinity contains -Infinity and +Infinity in JavaScript.";
let str2 = "My grandfather is 65 years old and My grandmother is 63 years old.";
let str3 = "The contract was declared null and void.";

console.log(str1.match("number"));   // "number" is a string. returns ["number"]
console.log(str1.match(NaN));        // the type of NaN is the number. returns ["NaN"]
console.log(str1.match(Infinity));   // the type of Infinity is the number. returns ["Infinity"]
console.log(str1.match(+Infinity));  // returns ["Infinity"]
console.log(str1.match(-Infinity));  // returns ["-Infinity"]
console.log(str2.match(65));         // returns ["65"]
console.log(str2.match(+65));        // A number with a positive sign. returns ["65"]
console.log(str2.match(85));         // returns null
console.log(str3.match(null));       // returns ["null"]