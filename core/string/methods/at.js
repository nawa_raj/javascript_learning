/*
    The at() method takes an integer value and returns a new String consisting of the 
    single UTF-16 code unit located at the specified offset. 
    This method allows for positive and negative integers. 
    Negative integers count back from the last string character.

    Syntax
        string.at(index)

    Return value
    A String consisting of the single UTF-16 code unit located at the specified position. 
    Returns undefined if the given index can not be found.

    it doesnot change the orginal string.
*/



console.log("\n------------ string.at() method --------------\n");

let sentence = 'The quick brown fox jumps over the lazy dog.';

console.log("orginal sentence: ", sentence);
console.log("sentence.at(13): ", sentence.at(13));
console.log("sentence.at(20): ", sentence.at(20));
console.log("sentence.at(100): ", sentence.at(100));
console.log("sentence.at(-4): ", sentence.at(-4));
console.log("sentence.at(-16): ", sentence.at(-16));
console.log("sentence.at(-90): ", sentence.at(-90));

console.log("Orginal String: ", sentence);
console.log();