/*
    concatination is the process to join two or more string value in a single string value.

    The concat() method concatenates the string arguments to the calling string and returns a new string.

    we can concat string using two different approach.

    1. using + operator: 
    2. using cancat method:

    Syntax:
        string.concat(str1, str2, ... , strN);
    
    Return value
    A new string containing the combined text of the strings provided.

    it doesnot modified orginal string.
*/


const str1 = 'Hello';
const str2 = 'World';

console.log("\n---------- string concat() method --------------\n");
console.log("orginal string1: ", str1);
console.log("orginal string2: ", str2);

console.log("now str1.concat(' ', str2): ", str1.concat(' ', str2));
console.log("now str2.concat(', ', str1): ", str2.concat(', ', str1));

console.log("\nnow after concatination\n");
console.log("orginal string1: ", str1);
console.log("orginal string2: ", str2);
console.log();
