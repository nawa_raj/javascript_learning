/*
    The trim() method removes whitespace from both ends of a string and returns a new string,
    without modifying the original string. Whitespace in this context is all the whitespace 
    characters (space, tab, no-break space, etc.) and all the line terminator characters (LF, CR, etc.).

    Syntax:
        trim()

    Return Value:
    A new string representing str stripped of whitespace from both its beginning and end.
*/

console.log("\n------------- String.prototype.trim() -------------\n");

const greeting = '   Hello world!   ';
console.log("orginal string: ", `"${greeting}"`);
console.log("greeting.trim(): ", `"${greeting.trim()}"`);
console.log();




// String.prototype.trimEnd()
/*
    The trimEnd() method removes whitespace from the end of a string. 
    trimRight() is an alias of this method.

    Syntax:
        trimEnd()
        trimRight()

    Return Value:
    A new string representing str stripped of whitespace from its end (right side).
*/

console.log("\n------------- String.prototype.trimEnd() -------------\n");

console.log("orginal string: ", `"${greeting}"`);
console.log("greeting.trimEnd(): ", `"${greeting.trimEnd()}"`);
console.log();





// String.prototype.trimStart()
/*
    The trimStart() method removes whitespace from the beginning of a string.
     trimLeft() is an alias of this method.

    Syntax:
        trimStart()
        trimLeft()

    Return Value:
    A new string representing str stripped of whitespace from its end (right side).
*/

console.log("\n------------- String.prototype.trimStart() -------------\n");

console.log("orginal string: ", `"${greeting}"`);
console.log("greeting.trimStart(): ", `"${greeting.trimStar()}"`);
console.log(String.prototype.trimLeft.name === "trimStart");
console.log();