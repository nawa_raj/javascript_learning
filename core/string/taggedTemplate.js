/*
    it is one trick or way to code dynamically call the dynamic value and manupulate its string with 
    string literals. this help to manage and dynamically manupulate the string.
*/


// we have following case:

/*
    There are 5 movie tickets each Rs. 200 and if you bye 5 ticket at the time you get discount Rs. 50 
    on each ticket i.e Rs. 250 in aggregiate.

    There are 5 movie tickets each Rs. 200 and if you bye less then 5 ticket at the time you get no 
    discount on any ticket.
*/


var noOfTicket = 5;
var buyTicket = 5;
var eachPrice = 200;
var disPrice = 50;



// now we simply display this condition using string literals.
console.log("\n------------ String Literals/String Interpolation -------------\n")
console.log(`There are ${noOfTicket} movie tickets each price Rs. ${eachPrice} and if you buy ${buyTicket} ticket at the time you get discount Rs. ${disPrice} 
    on each ticket i.e Rs. ${buyTicket * disPrice} in aggregiate.`)
console.log();



// now we use tagged teplate to display same things.
console.log("\n\n\n-------------- Tagged Template -------------------\n");

function taggedTemplate(sentence, ...args) {
    if (args[2] < 5) {
        args[3] = 0;

        return `${sentence[0]} ${args[0]} ${sentence[1]} ${args[1]} ${sentence[2]} ${args[2]} ${sentence[3]} ${args[3]} ${sentence[4]} ${args[3] * args[1]} ${sentence[5]}`
    }
    return `${sentence[0]} ${args[0]} ${sentence[1]} ${args[1]} ${sentence[2]} ${args[2]} ${sentence[3]} ${args[3]} ${sentence[4]} ${args[4]} ${sentence[5]}`;
}


console.log(taggedTemplate`There are ${noOfTicket} movie tickets each price Rs. ${eachPrice} and if you buy ${buyTicket} ticket at the time you get discount Rs. ${disPrice} 
    on each ticket i.e Rs. ${buyTicket * disPrice} in aggregiate.`)
console.log();