



// switch case...
console.log();
console.log("---------- switch case -----------")
console.log();

const color = 20;

switch (color) {
    case "red":
        console.log("color is: ", color);
        break;

    case "blue":
        console.log("color is: ", color);
        break;

    case "green":
        console.log("color is: ", color);
        break;

    case "white":
        console.log("color is: ", color);
        break;

    default:
        console.log("color is not matched")
}
console.log();